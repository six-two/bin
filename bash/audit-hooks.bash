#!/usr/bin/false

# bash command logging:
# https://askubuntu.com/questions/93566/how-to-log-all-bash-commands-by-all-users-on-a-server
# https://spin.atomicobject.com/2016/05/28/log-bash-history/
# Easy solution, but needs bash 4.4: PS0="$(date) started\n"
# More complicated version with seemingly easier use: https://github.com/rcaloras/bash-preexec


mybins_pad_str() {
    if [[ $# -ne 2 ]]; then
        echo "[!] Usage: $0 <STRING> <MIN_LENGTH>" >&2
        return 1
    fi

    local STRING="$1"
    local MIN_LENGTH="$2"
    # See explanations at https://gist.github.com/mattmc3/110eca74a876154c842423471b8e5cbb
    if [[ "${#STRING}" -ge "$MIN_LENGTH" ]]; then
        # Padding also trims the string down. So to keep everything, we will just return it unmodified if it is longer
        echo -n "$STRING"
    else
        # Padd with spaces on the right side, until MIN_LENGTH is reached
        # https://stackoverflow.com/questions/5349718/how-can-i-repeat-a-character-in-bash
        # printf "$STRING"'%.s' $(eval "echo {1.."$(($MIN_LENGTH))"}")
        
        DIFF_LENGTH=$(($MIN_LENGTH-${#STRING}))
        echo -n "$STRING"
        printf "%${DIFF_LENGTH}s"
    fi
}

audit_log_message() {
    if [[ $# -ne 2 ]]; then
        echo "[!] Usage: $0 <ACTION> <COMMAND>" >&2
        return 1
    fi

    if [[ ! -d ~/.audit ]]; then
        mkdir ~/.audit
    fi

    local file_name="$HOME/.audit/bash-history-$(date +%Y-%m-%d).log"
    local str_timestamp="$(date '+%Y-%m-%d %H:%M:%S')"
    local str_action="$(mybins_pad_str "$1" 25)"
    local str_shell_pid="\$\$=$(mybins_pad_str "$$" 6)"
    local str_cwd="\$PWD=$(mybins_pad_str "$PWD" 50)"

    echo "$str_timestamp | $str_shell_pid | $str_action | $str_cwd | $2" >> $file_name
}


# https://superuser.com/questions/471884/zsh-post-command-function-hook
# https://zsh.sourceforge.io/Doc/Release/Functions.html#index-preexec_005ffunctions

audit_cmd_stop() {
    local LAST_STATUS=$?
    # Note to self: I can not use [[ -v ... ]], since MacOS's bash is too old :/
    if [[ -n "$MYBINS_LAST_CMD" && "$MYBINS_AUDIT_ENABLED" -eq 1 ]]; then
        # In bash $SECONDS is always int. But even though $EPOCHREALTIME would work, bash has no native floating point math
        # Since reliability is more important than fractions of a second, we will just use $SECONDS
        local CMD_TIME_DIFF=$(($SECONDS-$MYBINS_LAST_START))
        # do not print this immediately after starting the audit
        audit_log_message "STOPPING in ${CMD_TIME_DIFF}s; \$?=${LAST_STATUS}" "$MYBINS_LAST_CMD"
    fi
    return 0  # Always return 'true' to avoid any hiccups.
}

audit_cmd_start() {
    if [[ "$MYBINS_AUDIT_ENABLED" -eq 1 ]]; then
        MYBINS_LAST_CMD="$1"
        MYBINS_LAST_START="$SECONDS"

        audit_log_message "STARTING" "$MYBINS_LAST_CMD"
    fi
    return 0  # Always return 'true' to avoid any hiccups.
}

audit_shell_exit() {
    audit_log_message "EXITING shell" ""
}

unregister_audit_hooks() {
    if [[ "$MYBINS_AUDIT_ENABLED" -ne 0 ]]; then
        MYBINS_AUDIT_ENABLED=0
        audit_log_message "AUDIT stopped" ""

        unset MYBINS_LAST_CMD
    fi
}

register_audit_hooks() {
    if [[ "$MYBINS_AUDIT_ENABLED" -ne 1 ]]; then
        MYBINS_AUDIT_ENABLED=1
        unset MYBINS_LAST_CMD

        audit_log_message "AUDIT started" ""
    fi
}


if [[ -n "$MYBINS_AUDIT" ]]; then
    # If you enable audit mode, all your commands will be logged.
    # Fish history can not be trusted, since it deletes duplicates (and does not tell you when a command ends)
    register_audit_hooks
fi

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
source "${SCRIPT_DIR}/bash-preexec.sh"

# Removing them dynamically seems hard, so I will just check a variable to see if it is currently enables
precmd_functions+=(audit_cmd_stop)
preexec_functions+=(audit_cmd_start)

