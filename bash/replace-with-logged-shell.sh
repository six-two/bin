# This file should be sourceable in bash or zsh

# Only do the replacement if it is not already started by screen (prevent recursions)
if ! is-logged-with-screen; then
  # Make sure that script is actually installed and in the path
  if ! command -v script &>/dev/null; then
    echo "[-] 'script' is not installed, so this session can not be recorded"
  else
    # File will be unique, since PID+time should not be duplicated
    base_path=~/.term-rec/"$(date +%Y-%m-%d)/$(date +%H-%M-%S)-$1-$$"
    mkdir -p "$(dirname "$base_path")"

    if [[ "$(uname)" == Linux ]]; then
      echo "[*] All output is logged to $base_path.log"
      # Replace this shell with a session logged using script
      exec script --quiet --command "$1" \
        --log-out "$base_path.log" \
        --log-timing "$base_path.time"
    elif [[ "$(uname)" == Darwin ]]; then
      echo "[*] All output is logged to $base_path.log"
      # There exists a option to record timestamps, but it shooshes everything into a single file -> grep may not work anymore
      # So currently I do not use the option
      #      -r      Record a session with input, output, and timestamping.
      exec script -q "$base_path.log" "$1"
    else
      echo "[-] Unknown operating system '$(uname)'. Logging is only supported on MacOS and Linux"
    fi
  fi
fi
