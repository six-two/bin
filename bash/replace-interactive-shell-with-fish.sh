# This file should be sourceable in bash or zsh
# It should work on Linux and macOS. To avoid breaking some tools, we allowlist all applications, where we want to replace the terminal

# ZSH is spawned by default in builtin terminals in applications like VS Code, Marta, Terminal, etc
# We want to replace it automatically with a fish shell (if fish is installed), so that we do no need to configure each application individually
# First check if we are in an interactive shell and if fish is installed
if [[ $- == *i* ]] && command -v fish &>/dev/null; then
    # Process ids are padded with spaces, which Linux does not like. So we do not escape the process id variables/subcommands
    PARENT_PID=$(ps -o ppid= -p $$ | tr -d " ")
    # On Linux the result is just a name, but on macOS it is the full path
    PARENT_PROCESS_NAME="$(ps -o comm= -p $PARENT_PID)" 
    GRANDPARENT_PROCESS_NAME="$(ps -o comm= -p $(ps -o ppid= -p $PARENT_PID))"

    # Now we check common names of applications with built in terminals
    case "$PARENT_PROCESS_NAME" in
        # Marta is a file manager with a builtin terminal
        /Applications/Marta.app/Contents/MacOS/Marta)
            exec fish
            ;;

        # Visual studio code also has built in terminals
        "/Applications/Visual Studio Code.app/Contents/Frameworks/Code Helper.app/Contents/MacOS/Code Helper")
            exec fish
            ;;
        
        # macOS terminal emulators
        # macOS is weird and does not spawn a login shell at login. Instead one is started only/whenever we start any terminal emulator. The terminal emulator calls login which calls our shell. So we need to check if the parent of login is the Terminal
        login|/usr/bin/login)
            if [[ "$GRANDPARENT_PROCESS_NAME" == /System/Applications/Utilities/Terminal.app/Contents/MacOS/Terminal ]]; then
                # Native macOS Terminal
                exec fish
            elif [[ "$GRANDPARENT_PROCESS_NAME" == /Applications/Ghostty.app/Contents/MacOS/ghostty ]]; then
                # Ghostty
                exec fish
            fi
            ;;

        # Linux Terminal (XFCE4 desktop)
        xfce4-terminal)
            exec fish
            ;;

        # My favorite Linux terminal emulator
        alacritty)
            exec fish
            ;;

        # Default Kali Linux terminal emulator
        qterminal)
            exec fish
            ;;

        # Visual studio code appears as electron, so we check the grandparent
        electron)
            if [[ "$GRANDPARENT_PROCESS_NAME" == code-oss ]]; then
                exec fish
            fi
            ;;

    esac
fi
