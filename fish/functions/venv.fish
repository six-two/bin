function venv
    # This needs to be a function, since we need to 'source' the venv -> not possible with a bash script
    set -f DIR (pwd)

    # use a max depth in case I screw up the code
    for ingored in $(seq 50)
        if test -f "$DIR/venv/bin/activate.fish"
            # We found a directory containing a virtual environment
            echo "[*] Sourcing $DIR/venv/bin/activate.fish"
            source "$DIR/venv/bin/activate.fish"
            return 0
        else if test "$DIR" -ef /
            # We reached the root directory without finding a venv. Ask user if it should be created
            echo "[-] No virtual environment found in any of the parent directories"
            while true
                read -l -P '[?] Create venv in '(pwd)'? (y/n) ' confirm
                switch $confirm
                case Y y
                    venv-create
                    return 0
                case N n
                    return 1
                case '*'
                    echo "Please answer (y)es or (n)o";;
                end
            end
        else
            set -f DIR (dirname $DIR)
        end
    end

    echo "[!] Bug: This code should not be reached (unless you are in a very deeply nested directory)"
end
