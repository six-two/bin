function my_left_prompt --description "Generates the string to show as the left prompt"
    function print_segment
        my_custom_prompt_segment "───[" "]" $argv
    end

    # Add an empty line to better visually separate it from the last command
    echo -en "┌"

    # Show something like a hostname
    if [ -n "$FISH_PROMPT_NAME" ]
        print_segment yellow "$FISH_PROMPT_NAME"
    end


    # Show the current working directory. Green means the last command exited normally, red means there was an error
    if [ $argv[1] -eq 0 ]
        set color 0d0
    else
        set color d00
    end
    print_segment $color (prompt_pwd)

    # Show git status, it provides its own colors, so we pass "normal" as color
    print_segment normal (fish_vcs_prompt | sed -e "s/[()]//g")

    # Search for local todo files in all parent directories, if they are found, show the number of todos
    set -l todo_text ""
    set -f dir (pwd)
    for i in (seq 50) # Prevent endless loops in case of programming errors
        if test -f $dir/.todos
            set -l todo_count (grep "^\s*-" $dir/.todos | wc -l)
            if test $todo_count -eq 0
                : # do nothing, since no todos exist
            else if test $todo_count -eq 1
                set -f todo_text "1 TODO"
            else
                set -f todo_text "$todo_count TODOs"
            end
            # We have found the most spacific todo file
            break
        else
            set -f parent_dir (dirname $dir)
            if test $dir = $parent_dir
                # we have reached the root directory
                break
            else
                set -f dir $parent_dir
            end
        end
    end
    print_segment yellow $todo_text


    # If recording, show an indicator
    print_segment --bold fff -b f00 "$tty_recorder"
    # if fish_is_root_user
end

function my_right_prompt --description "Generates the string to show as the left prompt"
  set -l last_pipestatus $argv[1]

  function print_segment
      my_custom_prompt_segment "[" "]───" $argv
  end

  set -l pipestatus_string (__fish_print_pipestatus "" "" "|" (set_color $fish_color_status) (set_color --bold $fish_color_status) $last_pipestatus)

  print_segment normal $pipestatus_string

  # Command duration
  if [ -n "$CMD_DURATION" -a "$CMD_DURATION" -gt 30 ]
    if [ "$CMD_DURATION" -lt 1000 ]
      print_segment green "$CMD_DURATION ms"
    else if [ "$CMD_DURATION" -lt 60000 ]
      print_segment yellow (math -s1 $CMD_DURATION/1000)'s'
    else
      print_segment red (math -s0 $CMD_DURATION/60000)'m '(math -s0 "($CMD_DURATION%60000)/1000")'s'
    end
  end

  # Timestamp
  my_custom_prompt_segment "[" "]" --bold blue (date '+%Y-%m-%d %H:%M:%S')
end


function my_prompt_add_padding --description "If necessary insert some padding"
    # Usage: <wanted-length> <current-length> <paddingchar>
    set -l wanted_len $argv[1]
    set -l current_len $argv[2]
    set -l padding_char $argv[3]

    if test $current_len -lt $wanted_len
        set -l padding_len (math $wanted_len - $current_len)
        # We use --max instead of --count in case we get a multi character padding string
        string repeat --no-newline --max $padding_len $padding_char
    end
end

function fish_prompt --description 'Write out the prompt'
    set -l last_status $status
    set -l last_pipestatus $pipestatus

    use_fish_defaults_for_undefined_variables

    set -l left (my_left_prompt $last_status)
    set -l right (my_right_prompt $last_pipestatus)

    set -l left_len (string length --visible "$left")
    set -l right_len (string length --visible $right)

    set -l prompt_len (math $left_len + $right_len)
    set -l MAX_WIDTH (math $COLUMNS - 1)

    set -l last_line "└─$PRETTY_SHELL_STACK-> "


    # If the combined prompt does not fit into one line, each part will get its own line
    if test $prompt_len -lt $MAX_WIDTH
        # Fits in a single line
        set -l padding (my_prompt_add_padding $MAX_WIDTH $prompt_len "─")
        echo -e "\n$left$padding$right\n$last_line"
    else
        ## Print it in two lines
        set -l left_padding_max_len (math $MAX_WIDTH - 1)
        set -l left_padding (my_prompt_add_padding $left_padding_max_len $left_len "─")"┐"
        set -l left_end " "

        # Add the padding in front of the right prompt
        if test $right_len -lt $MAX_WIDTH
            # -1 because we start the line with an extra character "├"
            set -l right_padding_max_len (math $MAX_WIDTH - 2)
            set right_padding "│"(my_prompt_add_padding $right_padding_max_len $right_len " ")
            set right_end "┘"
        else
            # The last character is always cut off, when the prompt is too long. So we add a useless extra character after the prompt
            set right_end " "
        end
        echo -e "\n$left$left_padding$left_end\n$right_padding$right$right_end\n$last_line"
    end
end

