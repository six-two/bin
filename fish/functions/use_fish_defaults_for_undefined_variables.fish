function use_fish_defaults_for_undefined_variables
    # Only run this function once per instance
    if set -q __use_fish_defaults_for_undefined_variables__
        return
    end
    set -g __use_fish_defaults_for_undefined_variables__ "Already executed"

    # Set up variables for the git prompt
    if not set -q __fish_git_prompt_show_informative_status
        set -g __fish_git_prompt_show_informative_status 1
    end
    if not set -q __fish_git_prompt_hide_untrackedfiles
        set -g __fish_git_prompt_hide_untrackedfiles 1
    end
    if not set -q __fish_git_prompt_color_branch
        set -g __fish_git_prompt_color_branch magenta --bold
    end
    if not set -q __fish_git_prompt_showupstream
        set -g __fish_git_prompt_showupstream "informative"
    end
    if not set -q __fish_git_prompt_char_upstream_ahead
        set -g __fish_git_prompt_char_upstream_ahead "↑"
    end
    if not set -q __fish_git_prompt_char_upstream_behind
        set -g __fish_git_prompt_char_upstream_behind "↓"
    end
    if not set -q __fish_git_prompt_char_upstream_prefix
        set -g __fish_git_prompt_char_upstream_prefix ""
    end
    if not set -q __fish_git_prompt_char_stagedstate
        set -g __fish_git_prompt_char_stagedstate "●"
    end
    if not set -q __fish_git_prompt_char_dirtystate
        set -g __fish_git_prompt_char_dirtystate "✚"
    end
    if not set -q __fish_git_prompt_char_untrackedfiles
        set -g __fish_git_prompt_char_untrackedfiles "…"
    end
    if not set -q __fish_git_prompt_char_invalidstate
        set -g __fish_git_prompt_char_invalidstate "✖"
    end
    if not set -q __fish_git_prompt_char_cleanstate
        set -g __fish_git_prompt_char_cleanstate "✔"
    end
    if not set -q __fish_git_prompt_color_dirtystate
        set -g __fish_git_prompt_color_dirtystate blue
    end
    if not set -q __fish_git_prompt_color_stagedstate
        set -g __fish_git_prompt_color_stagedstate yellow
    end
    if not set -q __fish_git_prompt_color_invalidstate
        set -g __fish_git_prompt_color_invalidstate red
    end
    if not set -q __fish_git_prompt_color_untrackedfiles
        set -g __fish_git_prompt_color_untrackedfiles $fish_color_normal
    end
    if not set -q __fish_git_prompt_color_cleanstate
        set -g __fish_git_prompt_color_cleanstate green --bold
    end


    # Set some default variables
    if not set -q DISPLAY_NAME_FILE
        set -g DISPLAY_NAME_FILE $HOME/.config/displayname
    end

    # Have something like the standard user@host
    if not set -q FISH_PROMPT_NAME
        if test -e $DISPLAY_NAME_FILE
            set -g FISH_PROMPT_NAME (cat $DISPLAY_NAME_FILE)
        else
            set -g FISH_PROMPT_NAME (whoami)@(hostname)
        end
    end


    # Set the program to use for the Arch User Repository
    if not set -q AURMANAGER
        set -gx AURMANAGER pikaur
    end

    # Autodetect the "best" version of vi-like text editors
    if not set -q VI
        if type -q nvim
            set -g VI nvim
        else if type -q vim
            set -g VI vim
        else if type -q vi
            set -g VI vi
        else
            # Fall back to nano
            set -g VI nano
        end
    end

    if not set -q EDITOR
        if type -q nano
            set -gx EDITOR nano
        else
            set -gx EDITOR $VI
        end
    end
end