function autocd-here --description "Automatically open new (fish) shells in this directory"
    set -Ux MYBINS_AUTO_CD (pwd)
end
