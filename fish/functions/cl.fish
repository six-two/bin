function cl --wraps='cd' --description 'Change to a folder and list its contents (long list)'
  cd $argv && ls -al; 
end
