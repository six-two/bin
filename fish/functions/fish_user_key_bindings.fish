function fish_user_key_bindings
	# fzf_key_bindings (provided by fzf) may not always be installed, so we check before calling it
	if type fzf_key_bindings &> /dev/null
		fzf_key_bindings
	end
end
