function venv-create
    if test -d venv
        while true
            read -l -P '[?] Directory "venv" already exists. Overwrite it? (y/n) ' confirm
            switch $confirm
            case Y y
                # Continue with the creation logic
                rm -r venv
                break
            case N n
                # Bail out early
                return 1
            case '*'
                echo "Please answer (y)es or (n)o";;
            end
        end
    end

    echo "[*] Creating virtual python environment"
    python3 -m venv --clear --upgrade-deps venv
    echo "[*] Sourcing venv/bin/activate.fish"
    source venv/bin/activate.fish  
    return 0
end
