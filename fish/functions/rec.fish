function rec --description 'Record a terminal session'
  set date_string (date +'%Y-%m-%d_%H-%M-%S')
  set base_path "$HOME/term-rec/$date_string"
  mkdir -p "$base_path"

  set replay_script '#!/bin/bash
# Replay the shell session recording that is in the same directory as this script
SCRIPT_DIR=$(dirname ${BASH_SOURCE[0]})
scriptreplay --log-out "${SCRIPT_DIR}/stdout" --log-timing "${SCRIPT_DIR}/stdout.time"'

  echo -e "$replay_script" > "$base_path/replay.sh"
  chmod +x "$base_path/replay.sh"

  script --command "fish -C 'set tty_recorder rec'" \
         --log-out "$base_path/stdout" \
         --log-timing "$base_path/stdout.time"
end
