function record-session-with-scl

    function print_headline
        echo "set_color -o blue; echo '[*]" (string join " " $argv)"'; set_color normal"
    end


    # Store the commands to run in the subshell in a list
    set --local init_cmd

    # notify the subsehll, that it is being recorded
    set -a init_cmd 'set -gx tty_recorder scl'

    set -a init_cmd (print_headline Local MAC addresses)
    set -a init_cmd 'ip -oneline link show'
    set -a init_cmd echo # Trailing empty line

    set -a init_cmd (print_headline Local IP addresses)
    set -a init_cmd 'ip -oneline address show'
    set -a init_cmd echo # Trailing empty line

    set -a init_cmd (print_headline Public IP address)
    set -a init_cmd 'curl https://ifconfig.me/ip'
    set -a init_cmd echo # The curl command does not end with a '\n' if it succeeds, so add it here
    set -a init_cmd echo # Trailing empty line

    set -a init_cmd (print_headline Enabling scl aliases)
    set -a init_cmd 'scl alias --print fish 2>/dev/null | source'
    set -a init_cmd echo # Trailing empty line

    # Log a whole session. Execute the initcommands we set up above, before the interactive session is running.
    scl log fish --interactive --init-command=(string join "; " $init_cmd)
end
