function pjurl --description 'Perform a curl request through Burpsuite and prettify the response with jq'
    purl -H 'Accept: application/json' 2>/dev/null $argv | jq .
end