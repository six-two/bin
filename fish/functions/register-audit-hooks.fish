function register-audit-hooks --description "log executed commands using events like fish_preexec"
    # If you enable audit mode, all your commands will be logged.
    # Fish history can not be trusted, since it deletes duplicates (and does not tell you when a command ends)
    # Running this function again will overwrite the functions, so they will be updated
    # This uses event listeners, see here: https://fishshell.com/docs/current/language.html#event
    
    function audit_fish_preexec --argument-names command_string --on-event fish_preexec --description "Log when command is started"
        audit_log_message "STARTING command" $command_string
    end

    function audit_fish_postexec --argument-names command_string --on-event fish_postexec --description "Log when command is stopped"
        set -l old_status $status
        set -l cmd_seconds (math -s1 $CMD_DURATION/1000)s
        audit_log_message "FINISHED in $cmd_seconds; \$?=$old_status" $command_string
    end

    function audit_fish_posterror --argument-names command_string --on-event fish_posterror --description "Log when command has syntax error"
        audit_log_message "SYNTAX ERROR in command" $command_string
    end

    function audit_fish_cancel --on-event fish_cancel --description "Log when line is canceled (Ctrl-C)"
        audit_log_message "CLEARING command line"
    end

    function audit_fish_exit --on-event fish_exit --description "Log when fish session terminates"
        audit_log_message "EXITING fish session"
    end

    function audit_log_message --argument-names log_action log_description --description "Write a message to the audit file"
        if ! test -d ~/.audit
            mkdir ~/.audit
        end
        set -l file_name ~/.audit/fish-history-(date +%Y-%m-%d).log
        set -l str_timestamp (date '+%Y-%m-%d %H:%M:%S')
        set -l str_action (string pad --width 25 --right $log_action)
        set -l str_shell_pid '$$='(string pad --width 6 --right $fish_pid)
        set -l str_cwd '$PWD='(string pad --width 50 --right $PWD)
        echo "$str_timestamp | $str_shell_pid | $str_action | $str_cwd | $log_description" >> $file_name
    end

    function audit_log_delete --description "Deletes all shell audit logs"
        while true
            read -l -P 'Do you REALLY want to delete ~/.audit/? [y/N] ' confirm
        
            switch $confirm
              case Y y
                # @TODO: delete it securely (shred, srm, etc) if possible, since it may contain passwords
                echo "Deleting them now..."
                rm -r ~/.audit/
                return 0
              case '' N n
                echo "Nothing was done"
                return 1
            end
        end
    end

    function unregister-audit-hooks --description "Clear hooks set by register-audit-hooks"
        # Log the fact that we stop the auditing
        if functions --query audit_log_message
            audit_log_message "AUDIT stopped" ""
        end
        
        # Then delete the functions
        functions --erase audit_fish_preexec
        functions --erase audit_fish_postexec
        functions --erase audit_fish_posterror
        functions --erase audit_fish_cancel
        functions --erase audit_fish_exit
        functions --erase audit_log_message
        functions --erase audit_log_delete
    end

    audit_log_message "AUDIT started" ""
end
