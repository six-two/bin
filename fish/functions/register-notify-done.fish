function register-notify-done --description "show a notification if a long running process finishes"
    # Running this function again will overwrite the functions, so they will be updated
    # This uses event listeners, see here: https://fishshell.com/docs/current/language.html#event

    # @TODO: check if OS supports notifications

    if test (count $argv) -ge 2
        echo "Usage: $0 [SECONDS_THRESHOLD]"
        return 1
    end

    if test -n "$argv[1]"
        if test "$argv[1]" -ge 0
            set -g NOTIFY_DONE "$argv[1]"
        else
            echo "Expected a positive number as argument 1"
            return 1
        end
    else if test -z "$NOTIFY_DONE"
        set -g NOTIFY_DONE 3
    end
    echo "[notify-done] Will show a notification if a process taking longer than $NOTIFY_DONE seconds finishes"

    function notify_done_postexec --argument-names command_string --on-event fish_postexec --description "Called when a command finishes"
        set -l old_status $status
        set -l cmd_seconds (math -s1 $CMD_DURATION/1000)

        if test $cmd_seconds -ge $NOTIFY_DONE
            if test $old_status -eq 0
                set -f msg_status "Finished"
            else
                set -f msg_status "Failed with code $old_status"
            end

            echo "[notify-done] Showing notification"
            send-notification "$msg_status after $cmd_seconds seconds" "$command_string"
        end
    end

    function unregister-notify-done --description "Clear hooks set by register-audit-hooks"
        # Delete the function(s) including itself
        functions --erase notify_done_postexec
        functions --erase unregister-notify-done
    end
end
