function jurl --description 'Perform a curl request and prettify the response with jq'
    curl -H 'Accept: application/json' 2>/dev/null $argv | jq .
end