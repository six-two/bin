function my-fishrc-init
    # On top to hide/prevent the following error message on MacOS with fish as the first shell:
    # SHELL_STACK not set and 'pstree' is not found
    if test -z "$SHELL_STACK"
        set -xg SHELL_STACK "fish"
    else
        set -xg SHELL_STACK "$SHELL_STACK fish"
    end

    if test -n "$MYBINS_RECORD" && ! is-logged-with-screen
        # This function will be executed once and is not important anyways -> perfect target for out run later code
        function fish_greeting
            if ! command -v script &>/dev/null
                echo "[-] 'script' is not installed, so this session can not be recorded"
            else
                # File will be unique, since PID+time should not be duplicated
                set -l base_path ~/.term-rec/(date +%Y-%m-%d)/(date +%H-%M-%S)-fish-"$fish_pid"
                mkdir -p (dirname $base_path)

                switch (uname)
                    case Linux
                        echo "[*] All output is logged to $base_path.log"

                        # Replace this shell with a session logged using script
                        # this needs to run after the init file, otherwise event handlers (for ctls-z for example) will not be set up yet (IDK why)
                        exec script --quiet --command fish \
                            --log-out "$base_path.log" \
                            --log-timing "$base_path.time"
                    case Darwin # MacOS
                        echo "[*] All output is logged to $base_path.log"
                        # There exists a option to record timestamps, but it shooshes everything into a single file -> grep may not work anymore
                        # So currently I do not use the option
                        #      -r      Record a session with input, output, and timestamping.
                        exec script -q "$base_path.log" fish
                    case '*'
                        echo "[-] Unknown operating system '"(uname)"'. Logging is only supported on MacOS and Linux"
                end
            end
        end
        # Return immediately, since the other stuf is not needed -> this process will never be interactively used
        return
    end

    # Get the shell stack if possible. Otherwise just show "fish"
    # This is used for my custom prompt
    if command -v "prompt-shell-stack" &>/dev/null
        set -g PRETTY_SHELL_STACK (prompt-shell-stack --block-format)
    else
        set -g PRETTY_SHELL_STACK "["(set_color yellow)"fish"(set_color normal)"]"
    end

    if test -n "$MYBINS_AUDIT"
        # If you enable audit mode, all your commands will be logged.
        # Fish history can not be trusted, since it deletes duplicates (and does not tell you when a command ends)
        register-audit-hooks
    end


    if test -n "$MYBINS_AUTO_CD"
        if test "$PWD" = "$HOME" # ensure we do not autocd if our terminal is explicitly opened in a non-default folder
            if command -v "prompt-shell-stack" &>/dev/null
                if prompt-shell-stack --is-first
                    if test -d "$MYBINS_AUTO_CD"
                        cd $MYBINS_AUTO_CD
                    else
                        echo "[-] MYBINS_AUTO_CD does not point to a directory: $MYBINS_AUTO_CD"
                    end
                end
            else
                if test "$SHELL_STACK" = "fish"
                    if test -d "$MYBINS_AUTO_CD"
                        cd $MYBINS_AUTO_CD
                    else
                        echo "[-] MYBINS_AUTO_CD does not point to a directory: $MYBINS_AUTO_CD"
                    end
                end
            end
        end
    end

    # Load VS Code terminal integration if we are in a code terminal
    # SEE https://code.visualstudio.com/docs/terminal/shell-integration#_installation
    # For safety's sake we should do it here again, since code usually thinks it spawns a system shell (bash or zsh).
    if string match -q "$TERM_PROGRAM" "vscode"
        . (code --locate-shell-integration-path fish)

        # Source the correct virtual environment (fish version, not bash version)
        function source
            set -l source_fish_path (path change-extension '.fish' $argv[1])
            if test $source_fish_path != $argv[1] && test -f $source_fish_path && string match -q '*/bin/activate.fish' $source_fish_path
                echo "[*] Rewrote '$argv[1]' -> $source_fish_path"
                builtin source $source_fish_path
            else
                builtin source $argv
            end
        end
    end
end
