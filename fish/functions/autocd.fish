function autocd --description "Switch to the default directory (MYBINS_AUTO_CD)"
    if test -n $MYBINS_AUTO_CD
        echo "[*] Switching to $MYBINS_AUTO_CD"
        cd $MYBINS_AUTO_CD
    else
        echo "[-] MYBINS_AUTO_CD not set, switching to home directory"
        cd
    end
end
