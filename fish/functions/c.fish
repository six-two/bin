function c --wraps='cd' --description 'Change to a folder and list its contents'
  cd $argv && ls; 
end
