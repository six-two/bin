function my_custom_prompt_segment
    # Usage: <prefix> <suffix> <color_arg> [...more_color_args] <text>
    # If <text> is not empty, prints "<prefix><text_with_color_args_applied><suffix>"
    if test (count $argv) -ge 4
        set -l segment_prefix $argv[1]
        set -l segment_suffix $argv[2]
        set -l color_args $argv[3..-2]
        set -l segment_text (string trim "$argv[-1]")

        # Only print something if a non-empty value is provided
        if test -n "$segment_text"
            echo -n "$segment_prefix"
            set_color $color_args
            echo -n "$segment_text"
            set_color normal
            echo -n "$segment_suffix"
        end
    end
end