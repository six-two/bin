# Zsh script dir variant, seems to work fine
SCRIPT_DIR="$( cd -- "$( dirname -- "${(%):-%x}" )" &> /dev/null && pwd )"

if [[ -n "$MYBINS_FISH_EVERYWHERE" ]]; then
  source "$SCRIPT_DIR/../bash/replace-interactive-shell-with-fish.sh"
fi

if [[ -n "$MYBINS_RECORD" ]] && ! is-logged-with-screen; then
  source "$SCRIPT_DIR/../bash/replace-with-logged-shell.sh" zsh
fi


if [[ $- == *i* ]]; then
    # Add zsh to the shell stack
    if [[ -z "${SHELL_STACK}" ]]; then
        export SHELL_STACK="zsh"
    else
        export SHELL_STACK="${SHELL_STACK} zsh"
    fi
fi

if [[ -n "$MYBINS_AUTO_CD" ]]; then
    if [[ "$PWD" = "$HOME" ]]; then # ensure we do not autocd if our terminal is explicitly opened in a non-default folder
        if command -v "prompt-shell-stack" &>/dev/null; then
            if prompt-shell-stack --is-first; then
                if test -d "$MYBINS_AUTO_CD"; then
                    cd $MYBINS_AUTO_CD
                else
                    echo "[-] MYBINS_AUTO_CD does not point to a directory: $MYBINS_AUTO_CD"
                fi

            fi
        else
            if [[ "$SHELL_STACK" == "zsh" ]]; then
                if test -d "$MYBINS_AUTO_CD"; then
                    cd $MYBINS_AUTO_CD
                else
                    echo "[-] MYBINS_AUTO_CD does not point to a directory: $MYBINS_AUTO_CD"
                fi
            fi
        fi
    fi
fi


# Source my prompt theme. This needs to be after updating the shell stack
. "${SCRIPT_DIR}/custom-prompt.zsh"
. "${SCRIPT_DIR}/audit-hooks.zsh"

# Change dirs without cd (like in fish)
setopt autocd

############## Start: Some options from kali #############
# These options are taken from ~/.zshrc of a 2022 Kali installation

# History configurations
HISTFILE=~/.zsh_history
HISTSIZE=1000
SAVEHIST=2000
setopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_ignore_dups       # ignore duplicated commands history list
setopt hist_ignore_space      # ignore commands that start with space
setopt hist_verify            # show command with history expansion to user before running it
#setopt share_history         # share command history data

# force zsh to show the complete history
alias history="history 0"


setopt interactivecomments # allow comments in interactive mode
setopt magicequalsubst     # enable filename expansion for arguments of the form ‘anything=expression’
setopt nonomatch           # hide error message if there is no match for the pattern
setopt notify              # report the status of background jobs immediately
setopt numericglobsort     # sort filenames numerically when it makes sense

# enable color support of ls, less and man, and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    export LS_COLORS="$LS_COLORS:ow=30;44:" # fix ls color for folders with 777 permissions

    export LESS_TERMCAP_mb=$'\E[1;31m'     # begin blink
    export LESS_TERMCAP_md=$'\E[1;36m'     # begin bold
    export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
    export LESS_TERMCAP_so=$'\E[01;33m'    # begin reverse video
    export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
    export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
    export LESS_TERMCAP_ue=$'\E[0m'        # reset underline

    # Take advantage of $LS_COLORS for completion as well
    zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
    zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
fi

# enable command-not-found if installed
if [ -f /etc/zsh_command_not_found ]; then
    . /etc/zsh_command_not_found
fi
#################### END Kali #####################

# What to show when the last line has no trailing \n
PROMPT_EOL_MARK="⇦"

FAILED_TO_SOURCE_EXTRA_FILES=0

source_additional_file() {
  # Usage: <FILE_NAME> <LIKELY_PATH> [...MORE_LIKELY_PATHS]
  # Tries to source the given file. First the given likely locations are searched.
  # If the file is not found, locate will be used to try to find the file.
  # If all fails, the FAILED_TO_SOURCE_EXTRA_FILES flag will be set

  if [[ $# -lt 2 ]]; then
    echo "Usage: source_additional_file <FILE_NAME> <LIKELY_PATH> [...MORE_LIKELY_PATHS]"
    return 2
  fi

  local NAME=$1
  shift

  for DIR in "$@"; do
    if [[ -f "${DIR}/${NAME}" ]]; then
      source "${DIR}/${NAME}"
      return 0
    fi
  done

  # #  Canceled for now, because it may help lead to privilege escalation attacks.
  # # @TODO: Maybe gate it behind a variable flag?
  # TRY_FIND_IT="$(locate --basename --limit 1 --literal zsh "${NAME}")"
  FAILED_TO_SOURCE_EXTRA_FILES=1
  if [[ "${MYZSH_DEBUG}" == 1 ]]; then
    echo "[D] Failed to source ${NAME}"
  fi
  return 1
}

### Autosuggestion. Default paths: Arch, Kali, local
source_additional_file zsh-autosuggestions.zsh \
    /usr/share/zsh/plugins/zsh-autosuggestions \
    /usr/share/zsh-autosuggestions \
    "${SCRIPT_DIR}/repos/zsh-autosuggestions"


### Syntax highlighting. Default paths: Arch, Kali, local
source_additional_file zsh-syntax-highlighting.zsh \
    /usr/share/zsh/plugins/zsh-syntax-highlighting \
    /usr/share/zsh-syntax-highlighting \
    "${SCRIPT_DIR}/repos/zsh-syntax-highlighting"


### Fish like history search
if source_additional_file zsh-history-substring-search.zsh \
      /usr/share/zsh/plugins/zsh-history-substring-search/ \
      "${SCRIPT_DIR}/repos/zsh-history-substring-search"; then
  # Overwrite the default function of the up and down buttons
  # This works on my mac and linux
  bindkey '^[[A' history-substring-search-up
  bindkey '^[[B' history-substring-search-down
  # This is required to make it work in my Kali VM (on Mac)
  bindkey "$terminfo[kcuu1]" history-substring-search-up
  bindkey "$terminfo[kcud1]" history-substring-search-down
fi

if [[ "${FAILED_TO_SOURCE_EXTRA_FILES}" -eq 1 ]]; then
  echo "[-] my-zshrc failed to load some plugins. Start as MYZSH_DEBUG=1 zsh to show details."
fi

# For Kali: Change title from ${debian_chroot:+${debian_... to just the current directory
# @TODO: GLITCH: It causes a flash when commands like 'sleep 1' finish
fix_tab_title() {
    print -Pn "\e]0;$PWD\a"
}
add-zsh-hook precmd fix_tab_title


# Run the autostart file (if it was not already run)
# Not needed on macos, since on macos you need a service and starting it will create a spam "[4]  + exit 1     autostart-launcher &> /dev/null" message
if [[ -o login ]] && [[ -f ~/.autostart ]] && [[ "$(uname)" != Darwin ]]; then
    autostart-launcher &>/dev/null &
fi
