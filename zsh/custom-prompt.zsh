#!/usr/bin/false
# Wanted looks:

#### Large screen size
# ┌──[REDACTED@hostname]───[/home/REDACTED/c/bin]───[git (main|-)]────────────────────────────────────────────────────[2022-11-19 20:50:40]
# └─ zsh >

#### Medium screen size
# ┌──[REDACTED@hostname]───[/home/REDACTED/c/bin]───[git (main|-)]┐
# │                                          [2022-11-19 20:45:58]┘
# └─ zsh >

#### Tiny screen size
# ┌──[REDACTED@hostname]───[/home/REDACTED/c/bin]───[
# git (main|-)]────────────────[2022-11-19 20:48:34]
# └─ zsh >


########################### Start of foreign code ##############################3
# @SOURCE SEE https://gist.github.com/romkatv/2a107ef9314f0d5f76563725b42f7cab

# Assumes that `%{%}` and `%G` don't lie.
function prompt-length() {
  emulate -L zsh
  local -i COLUMNS=${2:-COLUMNS}
  local -i x y=${#1} m
  if (( y )); then
    while (( ${${(%):-$1%$y(l.1.0)}[-1]} )); do
      x=y
      (( y *= 2 ))
    done
    while (( y > x + 1 )); do
      (( m = x + (y - x) / 2 ))
      (( ${${(%):-$1%$m(l.x.y)}[-1]} = m ))
    done
  fi
  typeset -g REPLY=$x
}

# Usage: fill-line LEFT RIGHT
#
# Sets REPLY to LEFT<spaces>RIGHT with enough spaces in
# the middle to fill a terminal line.
function fill-line() {
  emulate -L zsh
  prompt-length $1
  local -i left_len=REPLY
  prompt-length $2 9999
  local -i right_len=REPLY
  local -i pad_len=$((COLUMNS - left_len - right_len - ${ZLE_RPROMPT_INDENT:-1}))
  if (( pad_len < 1 )); then
    # My (six-two's) modification: Display both parts, make it look like the medium screen size example
    local -i left_pad_len=$((COLUMNS - left_len - 1 - ${ZLE_RPROMPT_INDENT:-1}))
    local left_pad=${(pl.$left_pad_len..─.)}  # pad_len dashes

    local -i right_pad_len=$((COLUMNS - right_len - 2 - ${ZLE_RPROMPT_INDENT:-1}))
    local right_pad=${(pl.$right_pad_len.. .)}  # pad_len spaces

    typeset -g REPLY=${1}${left_pad}$'┐\n│'${right_pad}${2}'┘'
  else
    local pad=${(pl.$pad_len..─.)}  # pad_len dashes
    typeset -g REPLY=${1}${pad}${2}
  fi
}

########################### End of foreign code ##############################3
# Get the shell stack if possible. Otherwise just show "zsh"
if command -v "prompt-shell-stack" &>/dev/null; then
  PRETTY_SHELL_STACK="$(prompt-shell-stack --block-format)"
else
  PRETTY_SHELL_STACK="$(print -P "[%F{yellow}zsh%f]")"
fi


# Sets PROMPT and RPROMPT.
#
# Requires: prompt_percent and no_prompt_subst.
function set-prompt() {
  emulate -L zsh
  vcs_info
  if [[ -n "${vcs_info_msg_0_}" ]]; then
    _vcs_block="───[${vcs_info_msg_0_}]"
  else
    _vcs_block=""
  fi

  local top_left="┌──[%F{yellow}%n@%m%f]───[%(?.%F{green}.%F{red})%/%f]${_vcs_block}"
  # @TODO: the conditional for the return code is problematic.
  # It mostly works now, but only for single digit response codes
  # local response_code="$(print -rP "%(?..%F{red}$response_code%f]───)")"
  local response_code="$(print -rP "%(?..[%F{red}%?%f]───)")"
  local top_right="${response_code}[%F{blue}%D{%Y-%m-%d %H:%M:%S}%f]"
  local bottom_left="└─${PRETTY_SHELL_STACK}-> "

  local NEWLINE=$'\n'
  local REPLY
  fill-line "$top_left" "$top_right"
  PROMPT="${NEWLINE}${REPLY}${NEWLINE}${bottom_left}"
  RPROMPT=""
}

setopt no_prompt_{bang,subst} prompt_{cr,percent,sp}
autoload -Uz add-zsh-hook
add-zsh-hook precmd set-prompt


# For git info
autoload -Uz vcs_info
zstyle ':vcs_info:*' formats '%s (%F{magenta}%b%f|%u%c%m)' # git(main)
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' unstagedstr '%F{red}-%f'
zstyle ':vcs_info:*' stagedstr '%F{green}+%f'
zstyle ':vcs_info:git*+set-message:*' hooks git-untracked

+vi-git-untracked() {
  if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] && \
     git status --porcelain | grep -m 1 '^??' &>/dev/null
  then
    hook_com[misc]='%F{yellow}?%f'
  fi
}
