mybins_pad_str() {
    if [[ $# -ne 2 ]]; then
        echo "[!] Usage: $0 <STRING> <MIN_LENGTH>" >&2
        return 1
    fi

    local STRING="$1"
    local MIN_LENGTH="$2"
    # See explanations at https://gist.github.com/mattmc3/110eca74a876154c842423471b8e5cbb
    if [[ "${#STRING}" -ge "$MIN_LENGTH" ]]; then
        # Padding also trims the string down. So to keep everything, we will just return it unmodified if it is longer
        echo -n "$STRING"
    else
        # Padd with spaces on the right side, until MIN_LENGTH is reached
        echo -n "${(r:$MIN_LENGTH:: :)STRING}"
    fi
}

audit_log_message() {
    if [[ $# -ne 2 ]]; then
        echo "[!] Usage: $0 <ACTION> <COMMAND>" >&2
        return 1
    fi

    if [[ ! -d ~/.audit ]]; then
        mkdir ~/.audit
    fi

    local file_name="$HOME/.audit/zsh-history-$(date +%Y-%m-%d).log"
    local str_timestamp="$(date '+%Y-%m-%d %H:%M:%S')"
    local str_action="$(mybins_pad_str "$1" 25)"
    local str_shell_pid="\$\$=$(mybins_pad_str "$$" 6)"
    local str_cwd="\$PWD=$(mybins_pad_str "$PWD" 50)"

    echo "$str_timestamp | $str_shell_pid | $str_action | $str_cwd | $2" >> $file_name
}


# https://superuser.com/questions/471884/zsh-post-command-function-hook
# https://zsh.sourceforge.io/Doc/Release/Functions.html#index-preexec_005ffunctions

audit_cmd_stop() {
    local LAST_STATUS=$?
    if [[ -v MYBINS_LAST_CMD ]]; then
        local CMD_TIME_DIFF=$(printf "%.1f" "$(($SECONDS-$MYBINS_LAST_START))")
        # do not print this immediately after starting the audit
        #@TODO: Determine run time
        audit_log_message "STOPPING in ${CMD_TIME_DIFF}s; \$?=${LAST_STATUS}" "$MYBINS_LAST_CMD"
    fi
    return 0  # Always return 'true' to avoid any hiccups.
}

audit_cmd_start() {
    # What value do I use? $1 may not exist, $2 may be incomplete, $3 may be very long, but is probably best
    MYBINS_LAST_CMD="$3"
    MYBINS_LAST_START="$SECONDS"

    audit_log_message "STARTING" "$MYBINS_LAST_CMD"
    return 0  # Always return 'true' to avoid any hiccups.
}

audit_shell_exit() {
    audit_log_message "EXITING shell" ""
}

unregister_audit_hooks() {
    audit_log_message "AUDIT stopped" ""

    add-zsh-hook -d precmd audit_cmd_stop
    add-zsh-hook -d preexec audit_cmd_start
    add-zsh-hook -d zshexit audit_shell_exit

    unset MYBINS_LAST_CMD
}

register_audit_hooks() {
    unset MYBINS_LAST_CMD

    autoload -Uz add-zsh-hook
    add-zsh-hook precmd audit_cmd_stop
    add-zsh-hook preexec audit_cmd_start
    add-zsh-hook zshexit audit_shell_exit

    audit_log_message "AUDIT started" ""
}

# Make the SECONDS variable (seconds since shell started) a float -> sub second accuracy
typeset -F SECONDS

if [[ -n "$MYBINS_AUDIT" ]]; then
    # If you enable audit mode, all your commands will be logged.
    # Fish history can not be trusted, since it deletes duplicates (and does not tell you when a command ends)
    register_audit_hooks
fi
