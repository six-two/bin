#!/usr/bin/env bash
# Simple AGE wrapper that encrypts or decrypts a file or stdin using the default keys (touch id + recovery)

RECOVERY_PUBLIC_KEY_FILE="$HOME/.config/age-recovery-pubkey.txt"
TOUCH_ID_KEY_FILE="$HOME/.config/age-plugin-se-key.txt"

is_installed() {
    command -v "$1" &>/dev/null
}

if ! is_installed age; then
    echo "[!] age is not installed. You can install it with 'brew install age'"
    exit 1
fi

if ! is_installed age-plugin-se; then
    echo "[!] age-plugin-se is not installed. You can install it with 'brew install age-plugin-se'"
    exit 1
fi

if ! [[ -f "$TOUCH_ID_KEY_FILE" ]]; then
    echo "[*] Touch ID key file did not exist. A new one will be created at $TOUCH_ID_KEY_FILE"
    age-plugin-se keygen --access-control=any-biometry -o "$TOUCH_ID_KEY_FILE" || exit 1
fi

if ! [[ -f "$RECOVERY_PUBLIC_KEY_FILE" ]]; then
    echo "[!] Recovery key file $RECOVERY_PUBLIC_KEY_FILE does not exist. You need to create it and safely store the private key on another device."
    echo "    This key will be necessary to restore your files if TouchID breaks or you need to access the files from another device."
    echo "[*] You can create one with the following command:"
    echo "    \$ age-keygen"
    echo "    Then write the public key (line looks like '# public key: age1...') to $RECOVERY_PUBLIC_KEY_FILE:"
    echo "    \$ echo age1... > $RECOVERY_PUBLIC_KEY_FILE"
    exit 1
fi

exit_with_message() {
    echo "[!] $@"
    exit 1
}

exit_if_file_already_exists() {
    # Usage: <FILE> <File type for description (like output, archive, input, etc)>
    if [[ -f "$1" ]]; then
        exit_with_message "The $2 file $(realpath "$1") already exists. Please rename or remove it"
    fi
}


if [[ $# -eq 1 ]]; then
    FILE="$(realpath "$1")"

    if [[ "$FILE" == *.age ]]; then
        echo "[*] Decrypting $FILE"

        [[ -f "$FILE" ]] || exit_with_message "$FILE does not exist or is not a normal file"

        # Remove .age file extension
        OUT_FILE="${FILE%????}"

        exit_if_file_already_exists "$OUT_FILE" output

        # decrypt somefile.txt.age -> somefile.txt
        age --decrypt --output "$OUT_FILE" --identity "$TOUCH_ID_KEY_FILE" "$FILE" || exit_with_message "Decrypting the file with age failed"

        if [[ "$OUT_FILE" == *.tgz || "$OUT_FILE" == *.tar.gz ]]; then
            # File is an archive, so we need to extract it after decryption
            echo "[*] Extracting $OUT_FILE"
            # Tar has relative paths so we need to jump there
            cd -- "$(dirname "$OUT_FILE")" || exit_with_message "cd to directory of $OUT_FILE failed"
            OUT_FILE="$(basename "$OUT_FILE")"
            # Unpack tar file to current directory
            #   -p    Restore permissions (including ACLs, owner, file flags)
            #   -k    Keep (don't overwrite) existing files
            tar -xz -p -k -f "$OUT_FILE" || exit_with_message "Failed to decompress the archive $(realpath "$OUT_FILE")"

            # remove the intermediate tar file
            rm "$OUT_FILE"
        fi
    else
        if [[ -d "$FILE" ]]; then
            echo "[*] Compressing $FILE"
            # Tar has relative paths so we need to jump there
            cd -- "$(dirname "$FILE")" || exit_with_message "cd to directory of $FILE failed"
            FILE="$(basename "$FILE")"

            exit_if_file_already_exists "$FILE.tgz" "intermediary archive"
            exit_if_file_already_exists "$FILE.tgz.age" output

            # Compress our folder
            tar -cz "$FILE" > "$FILE.tgz" || exit_with_message "Failed compressing the folder $FILE"

            # Make our other code run on the newly archived file
            FILE="$(realpath $FILE.tgz)"
            # After encryption, remove this file again
            FILE_TO_REMOVE="$FILE"
        fi

        if [[ -f "$FILE" ]]; then
            echo "[*] Encypting $FILE"
            RECOVERY_PUBLIC_KEY="$(cat "$RECOVERY_PUBLIC_KEY_FILE")"
            TOUCH_ID_PUBLIC_KEY="$(age-plugin-se recipients --input "$TOUCH_ID_KEY_FILE")"

            exit_if_file_already_exists "$FILE.age" output

            age --encrypt --output "$FILE.age" --recipient "$TOUCH_ID_PUBLIC_KEY" --recipient "$RECOVERY_PUBLIC_KEY" "$FILE"
            AGE_STATUS="$?"

            # Clean up (needed if it fails or succeeds)
            if [[ -n "$FILE_TO_REMOVE" ]]; then
                echo "[*] Removing intermediary file $FILE_TO_REMOVE"
                rm "$FILE_TO_REMOVE"
            fi

            if [[ "$AGE_STATUS" -ne 0 ]]; then
                exit_with_message "Failed to encrypt file with age"
            fi
        fi
    fi
else
    echo "[!] Usage: <FILE>"
    echo "If FILE ends with the .age extension, it will be decrypted. Otherwise the file or folder will be encrypted."
    exit 1
fi
