#!/usr/bin/env 62-bin-python
import argparse
import subprocess
# pip
import rumps

def hide_python_from_dock():
    try:
        from AppKit import NSBundle
        bundle = NSBundle.mainBundle()
        info = bundle.localizedInfoDictionary() or bundle.infoDictionary()
        info['LSUIElement'] = '1'
    except Exception as ex:
        print("[-] Error hiding Python from the Dock:", ex)


class MyApp(rumps.App):
    def __init__(self, show_max_chars, interval):
        super(MyApp, self).__init__("Clipboard Monitor", icon=None)
        self.show_max_chars = show_max_chars
        self.update_title(None)
        rumps.Timer(self.update_title, interval).start()

    @rumps.clicked("Clear")
    def clear_clipboard(self, _):
        subprocess.call(["bash", "-c", "true | pbcopy"])
        self.update_title(None)

    def update_title(self, _):
        try:
            clipboard = subprocess.check_output(["pbpaste"])
        except Exception as ex:
            self.title = f"Error reading clipboard: {ex}"
            return

        try:
            clipboard_text = clipboard.decode('utf-8')
        except Exception as ex:
            self.title = f"binary ({len(clipboard)})"
            return

        if len(clipboard_text) > self.show_max_chars:
            text = escape(clipboard_text[:self.show_max_chars])
            self.title = f"[{text}…] ({len(clipboard_text)})"
        else:
            text = escape(clipboard_text)
            self.title = f"[{text}]"


def escape(string):
    return string.replace("\n", " ").replace("\r", " ").replace("\0", "")


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-m", "--max", type=int, help="maximum amount of characters from clipboard to show (default: 15)", default=15)
    ap.add_argument("-i", "--interval", type=float, help="how many seconds to wait between updating the statusbar (default: 1.0)", default=1.0)
    args = ap.parse_args()

    # Hide the application from the dock before starting RUMPS
    hide_python_from_dock()

    # Start our RUMPS application
    MyApp(args.max, args.interval).run()
