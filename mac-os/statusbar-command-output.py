#!/usr/bin/env 62-bin-python
import argparse
import subprocess
# pip
import rumps

def hide_python_from_dock():
    try:
        from AppKit import NSBundle
        bundle = NSBundle.mainBundle()
        info = bundle.localizedInfoDictionary() or bundle.infoDictionary()
        info['LSUIElement'] = '1'
    except Exception as ex:
        print("[-] Error hiding Python from the Dock:", ex)


class MyApp(rumps.App):
    def __init__(self, show_max_chars, command, interval):
        super(MyApp, self).__init__("Command Monitor", icon=None)
        self.command = command
        # Show the configuration (for debugging)
        self.menu = [f"Command: {command}", f"Max output length: {show_max_chars}", f"Update every {interval} seconds"]
        self.show_max_chars = show_max_chars
        self.update_title(None)
        rumps.Timer(self.update_title, interval).start()


    def update_title(self, _):
        try:
            text = subprocess.check_output(self.command, shell=True).decode('utf-8').strip()
        except Exception as ex:
            text = str(ex)

        if len(text) > self.show_max_chars:
            text = escape(text[:self.show_max_chars])
            self.title = f"{text}…"
        else:
            text = escape(text)
            self.title = f"{text}"


def escape(string):
    return string.replace("\n", " ").replace("\r", " ").replace("\0", "")


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("command", help="command to run. For example: 'df / | sed 1d | awk \'{print $5}'\'")
    ap.add_argument("-m", "--max", type=int, help="maximum amount of characters from output to show (default: 25)", default=25)
    ap.add_argument("-i", "--interval", type=float, help="how many seconds to wait between updating the statusbar (default: 1.0)", default=1.0)
    args = ap.parse_args()

    # Hide the application from the dock before starting RUMPS
    hide_python_from_dock()

    # Start our RUMPS application
    MyApp(args.max, args.command, args.interval).run()
