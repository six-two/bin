#!/usr/bin/env bash
# This should undo the changes made by ./install.sh

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

ALIAS_FILES=("${SCRIPT_DIR}/my-aliases.source"
             "${SCRIPT_DIR}/generate_config_to_source")


for FILE in ~/.bashrc ~/.zshrc ~/.config/fish/config.fish; do
    if [[ -f "${FILE}" ]]; then
        for PATTERN in "${ALIAS_FILES[@]}"; do
            if [[ "${PATTERN}" == "*;*" || "${PATTERN}" == "*'*" ]]; then
                echo "The path may not contain a semicolon (;) or a single quote (')"
            else
                sed -i "\;${PATTERN};d" "${FILE}"
            fi
        done
    fi
done
