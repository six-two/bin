#!/usr/bin/env bash
# This will add a line that sources my aliases for the following shells:
# bash, fish, zsh

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

ALIAS_FILE="${SCRIPT_DIR}/generate_config_to_source"
if [[ "${ALIAS_FILE}" == "*|*" || "${ALIAS_FILE}" == "*'*" ]]; then
    echo "The path may not contain a semicolon (;) or a single quote (')"
fi

if [[ ! -f "${ALIAS_FILE}" ]]; then
    echo "BUG: Could not find alias file"
    exit 1
fi

is_installed() {
    command -v "$1" &>/dev/null
}

show_help_and_exit() {
    cat << EOF
Usage: <...flags>

Adds lines to most shell configuration files to source the output of ${ALIAS_FILE} <shellname> <flags>.
EOF
    exit 1
}

if [[ $# -eq 0 ]]; then
    show_help_and_exit
fi


function insert_or_update_line() {
    # Usage: <file> <shell> <args-string>

    if [[ ! -f "$1" ]]; then
        echo "[-] Skipping $1 because it does not exist"
        return
    fi 

    OUTPUT="$("${ALIAS_FILE}" $2 $3 2>&1)"
    if [[ $? -eq 0 ]]; then
        # To prevent that the line exists multiple times, delete all lines that mention the file
        if [[ $(uname -s) == Darwin ]]; then
            # Mac/BSD sed has a different syntax and requires an backup extension
            sed -i ".bak" "\|${ALIAS_FILE}|d" "$1"
        else
            # Normal GNU sed
            sed -i "\|${ALIAS_FILE}|d" "$1"
        fi
        
        # Then add the new line at the end
        if [[ "$2" == fish ]]; then
            # SEE https://fishshell.com/docs/current/cmds/status.html
            echo "status is-interactive && '${ALIAS_FILE}' $2 $3 | source" >> "$1"
        else
            # SEE https://www.gnu.org/software/bash/manual/bash.html#Is-this-Shell-Interactive_003f
            # Check for the interactive shell option
            echo "[[ \"\$-\" == *i* ]] && eval \"\$('${ALIAS_FILE}' $2 $3)\"" >> "$1"
        fi
    else
        echo "[!] Error: $2 command has an error, so it was not added to the bash config. Showing output below:"
        echo "${OUTPUT}"
    fi
}

##### Add a line that will source the alias file if an interactive shell is started

ARGS_STR=$@

insert_or_update_line ~/.bashrc                     bash    "${ARGS_STR}"
insert_or_update_line ~/.zshrc                      zsh     "${ARGS_STR}"
insert_or_update_line ~/.config/fish/config.fish    fish    "${ARGS_STR}"

# Update the python dependencies
if [[ -f "${SCRIPT_DIR}/venv/bin/activate" ]]; then
    echo "[*] Updating python dependencies"
    source "${SCRIPT_DIR}/venv/bin/activate"
    python3 -m pip install --upgrade pip
    python3 -m pip install -r "${SCRIPT_DIR}/requirements.txt"

    OS_NAME="$(uname | tr "[:upper:]" "[:lower:]")"
    OS_SPECIFIC_DEPENDENCIES="${SCRIPT_DIR}/requirements-${OS_NAME}.txt"
    if [[ -f "$OS_SPECIFIC_DEPENDENCIES" ]]; then
        echo "[*] Installing additional python dependencies for $OS_NAME"
        python3 -m pip install -r "$OS_SPECIFIC_DEPENDENCIES"
    else
        echo "[-] No additional python dependencies for $OS_NAME"
    fi
fi

if is_installed docker && is_installed fish; then
    if [[ ! -d ~/.config/fish/completions ]]; then
        mkdir -p ~/.config/fish/completions
    fi

    docker completion fish > ~/.config/fish/completions/docker.fish
fi
