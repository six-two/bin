#!/usr/bin/env python3
# This script clamps the value between 0 and 100 (wob's) default input and writes it to the wob pipe
import os
import sys


def parse_float(string: str) -> float:
    try:
        return float(percent_str)
    except:
        print(f"Not a number: '{percent_str}'")
        sys.exit(1)
        return 0  # to suppress a mypy warning


def clamp_int(min_value: int, value: float, max_value: int) -> int:
    if min_value > max_value:
        raise Exception(f"min_value ({min_value}) is bigger than max_value ({max_value})")

    if value < min_value:
        return min_value
    elif value > max_value:
        return max_value
    else:
        return round(value)


def write_to_file(path: str, value: int) -> None:
    if not os.path.exists(path):
        print(f"File does not exist: '{path}'")
        sys.exit(1)

    try:
        with open(path, "a") as f:
            f.write(f"{value}\n")
    except Exception as ex:
        print(f"Error writing to file: '{path}'\n{ex}")
        sys.exit(1)


if __name__ == "__main__":
    args = sys.argv[1:]
    argc = len(args)
    if argc > 1 and argc <= 2:
        print(f"Error: Got {argc} argument(s)")
        print(f"Usage: {sys.argv[0]} <wob_file> <percent_value>")
        sys.exit(1)

    wob_file = args[0]
    # Get value from commandline or stdin
    percent_str = args[1] if argc > 1 else sys.stdin.readline()

    percent_float = parse_float(percent_str.strip())
    percent_clamped = clamp_int(0, percent_float, 100)
    write_to_file(wob_file, percent_clamped)
