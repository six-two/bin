#!/usr/bin/env python3
# Currently designed to be a launcher for my ide, but should be more generic
# example usage:
# remember-last.py --file --state ~/.config/remember-last.py/vscode code %CHOICE%
import argparse
import json
import os
from pathlib import Path
import subprocess
from typing import Optional
import shutil

ADD_OPTION = "[Add entry]"
DELETE_OPTION = "[Delete entry]"
EDIT_VSCODE_OPTION = "[Edit list in Visual Studio Code]"
EXTRA_OPTIONS = [ADD_OPTION, EDIT_VSCODE_OPTION, DELETE_OPTION]

def dmenu_command(prompt: str) -> list[str]:
    # you can modify this method to make a dmenu (or bemenu or whatever) invocation with your preferred parameters

    # Preffer bemenu, since it has wayland and x11 backends
    if shutil.which("bemenu"):
        return ["bemenu", "--ignorecase", "--prompt", str(prompt), "--list", "15"]
    elif shutil.which("dmenu"):
        return ["dmenu", "-i", "-p", str(prompt), "-l", "15"]
    else:
        raise Exception("No dmenu like program found. Checked for 'dmenu' and 'bemenu'")


def dmenu_select(prompt: str, options: list[str]) -> str:
    dmenu_input = "\n".join(options)
    dmenu_input = dmenu_input.encode()

    dmenu_output = subprocess.check_output(dmenu_command(prompt), input=dmenu_input)
    if dmenu_output:
        return dmenu_output.decode().rstrip()
    else:
        return ""


class RememberLast:
    def __init__(self, state_file: str, dry_run: bool):
        self.state_file = state_file
        self.state: Optional[list[str]] = None
        self.dry_run = dry_run

    def _get_state(self) -> list[str]:
        if self.state == None:
            if os.path.exists(self.state_file):
                with open(self.state_file, "r") as file:
                    data = json.load(file)
                # data should be a list of strings, but i do not want to use external dependencies (like `schema`)
                data_list = [str(x) for x in data]

                # only allow items once, but keep their order
                # also delete the extra options
                self.state = []
                for item in data_list:
                    if item not in self.state and item not in EXTRA_OPTIONS:
                        self.state.append(item)
            else:
                # Create an empty file, if no previous data exists
                folder_name = os.path.dirname(self.state_file)
                os.makedirs(folder_name, exist_ok=True)
                self.state = []
                self._store_state()
                print(f"Created statefile '{self.state_file}'")

        return self.state

    def _store_state(self) -> None:
        if self.dry_run:
            print("[Dry run] Skipped state file write")
        else:
            if self.state == None:
                raise Exception("You need to load the state before you change it")
            with open(self.state_file, "w") as file:
                json.dump(self.state, file)

    def _choose_value_from_state(self, prompt: str, show_extra_options: bool) -> int:
        options = list(self._get_state())
        if show_extra_options:
            options.append(ADD_OPTION)
            options.append(EDIT_VSCODE_OPTION)
            if self.state:
                options.append(DELETE_OPTION)
        
        dmenu_output = dmenu_select(prompt, options)
        if dmenu_output:
            if dmenu_output == DELETE_OPTION:
                index = self._choose_value_from_state("Select the value to delete", False)
                del self.state[index]
                # Store the change. Would be really annoying if you do multiple changes and then it crashes not saving any of your changes :)
                self._store_state()
                # recursive calls
                return self._choose_value_from_state(prompt, show_extra_options)
            elif dmenu_output == ADD_OPTION:
                name = dmenu_select("Type the value you want to add:", [])
                if name:
                    self.state.insert(0, name)
                    return 0
                else:
                    raise Exception("You can not add an empty value")
            elif dmenu_output == EDIT_VSCODE_OPTION:
                subprocess.call(["code", self.state_file])
            elif dmenu_output in self.state:
                return self.state.index(dmenu_output)
            else:
                raise Exception("A unknown value was supplied")
        else:
            raise Exception("Empty dmenu output")

    def launch(self, command: list[str], placeholder: str, value_is_path: bool):
        description = f"Launch {command[0]}"
        index = self._choose_value_from_state(description, True)

        value = self.state[index]
        # Move the selected value to the top
        if index != 0:
            self.state.remove(value)
            self.state.insert(0, value)

        # Store the new state
        self._store_state()

        # Handle paths like ~/.config/
        if value_is_path:
            value = os.path.expanduser(value)

        # replace the placeholder
        command = [x.replace(placeholder, value) for x in command]
        print("[Command]", command)
        if self.dry_run:
            print("[Dry run] Skipped executing command")
        else:
            subprocess.call(command)
        


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--placeholder", nargs="?", default="%CHOICE%",
     help="the placeholder that will be replaced with the user supplied value (default: '%(default)s')")
    ap.add_argument("-r", "--dry-run", action="store_true",
     help="only print the command to the console, but do not execute it. Will also prevent the modified state file from being saved")
    ap.add_argument("-f", "--file", action="store_true", help="perform path expansion for ~")
    ap.add_argument("-s", "--state", required=True, help="the state file location")
    ap.add_argument("command", nargs="+", help="the command to be executed. Each parameter should be passed as a single parameter. If you don't like this, pass them like this 'bash -c \"your-command --flags parameters\"'")
    args = ap.parse_args()

    state_file = os.path.expanduser(args.state)
    remember_last = RememberLast(state_file, args.dry_run)
    try:
        remember_last.launch(args.command, args.placeholder, args.file)
    except KeyboardInterrupt:
        print("Interrupted by user")
    except Exception as ex:
        print(ex)
        exit(1)


def test():
    path = os.path.expanduser("~/.config/remember-last.py/test")
    RememberLast(path).launch(["code", "%CHOICE%"], "%CHOICE%", True)


if __name__ == "__main__":
    main()
