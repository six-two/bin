#!/usr/bin/bash
# This script restarts wob, if it crashes. This is needed to handle values bigger than 100% (eg 105% volume levels)
# Otherwise it would just stop working afterwards. The first argument is the pipe, to read data from.

if [ $# -ne 1 ]; then
    echo "Usage: $0 <pipe_file_path>"
    exit 1
fi

# If wob is not installed exit immediately
if ! [ -x "$(command -v git)" ]; then
    echo "Command 'wob' could not be found"
    exit 1
fi

echo "Starting with pipe '$1'"

if [ -p "$1" ]; then
    echo "Pipe already exists"
else
    echo "Creating pipe"
    # Remove the old file if it was not a pipe
    rm -f "$1"
    mkfifo "$1"
fi


until tail -f "$1" | wob; do
    echo "Process crashed with exit code $?.  Respawning.." > /tmp/wob.log
    sleep 1
done
