#!/usr/bin/env 62-bin-python
# Crop and scale images to fit in a certain size. Useful for automatically creating wallpapers from photos
import sys
import math
import os
import re
# pip packages
try:
    from PIL import Image
except:
    print("Please run 'pip install pillow'")
    sys.exit(1)


def folder_resize(input_folder, output_folder, size):
    for root, _, files in os.walk(input_folder):
        # Determine the path relative to input_folder
        if not root.startswith(input_folder):
            raise Exception(
                f"Expected '{root}' to start with '{input_folder}'")
        rel_path = root[len(input_folder):]
        # remove leading slashes
        rel_path = re.sub(r"^[/\\]*", "", rel_path)

        # create a output folder that mirrors the input folders naming scheme
        current_output_folder = os.path.join(output_folder, rel_path)
        os.makedirs(current_output_folder, exist_ok=True)

        for file_name in files:
            extension = file_name.split(".")[-1]
            if extension.lower() in ["jpg", "jpeg", "png", "gif"]:
                input_file = os.path.join(root, file_name)
                output_file = os.path.join(current_output_folder, file_name)
                img_file_resize(input_file, output_file, size)


def img_file_resize(input_path, output_path, size):
    """
    Resizes and crops an image to fit `size`. Useful side effect: It also clears the image metadata
    """
    image = Image.open(input_path)
    image = resize_keep_aspect_ratio(image, size)
    image = crop_center(image, size)
    image.save(output_path)


def resize_keep_aspect_ratio(image, target_min_size):
    """
    Resize a image so that one dimension matches target_min_size (or is slightly bigger), and the other one is larger
    """
    tw, th = target_min_size
    cw, ch = image.size

    ratio = max(tw/cw, th/ch)
    nw = int(math.ceil(cw*ratio))
    nh = int(math.ceil(ch*ratio))
    new_size = (nw, nh)
    print(new_size)
    return image.resize(new_size)


def crop_center(image, target_size):
    """
    Crops the image to target_size. The cropping is focused on the center
    """
    tw, th = target_size
    cw, ch = image.size

    if cw < tw or ch < th:
        raise Exception(
            f"Current size {image.size} smaller than target size {target_size}")

    # start
    sx = (cw - tw) // 2
    sy = (ch - th) // 2
    # end
    ex = sx + tw
    ey = sy + th

    box = (sx, sy, ex, ey)
    print(box)
    return image.crop(box)


def parse_size_string(size_str: str) -> tuple[int, int]:
    try:
        w_str, h_str = size_str.split("x")
        return (int(w_str), int(h_str))
    except:
        print(f"Not a valid size: '{size_str}'")
        print("Format: <width>x<height>")
        print("Example: 1920x1080")
        sys.exit(1)


if __name__ == "__main__":
    try:
        input_folder = sys.argv[1]
        output_folder = sys.argv[2]
        size = parse_size_string(sys.argv[3])
        folder_resize(input_folder, output_folder, size)
    except IndexError:
        print("Usage: <input_folder> <output_folder> <size>")
        print("Example: ~/photos/originals ~/photos/resized 1920x1080")
        sys.exit(1)
