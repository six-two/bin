#!/usr/bin/env python3
# Display Power Management Signaling Daemon
# -----------------------------------------
# This program periodically checks if the screen is locked.
# If it is, DPMS will be enabled, to turn the display off after a few seconds of inactivity.

import subprocess
import sys
import time
import traceback
from typing import Optional
# pip install psutil
import psutil

# Arguments for xset dpms. Times in seconds, 0 == disabled
DPMS_TIMES_LOCKED = (0, 5, 0)
DPMS_TIMES_UNLOCKED = (0, 0, 0)

# Put your screenlockers here
SCREEN_LOCKERS = ["i3lock", "slock"]


def main() -> None:
    is_locked = is_computer_locked()
    try:
        on_locked_state_change(is_locked)
        while True:
            new_is_locked = is_computer_locked()
            if new_is_locked != is_locked:
                on_locked_state_change(new_is_locked)
                is_locked = new_is_locked
            time.sleep(0.33)
    except KeyboardInterrupt:
        sys.exit()
        

def on_locked_state_change(is_locked: bool) -> None:
    print("State changed:", "locked" if is_locked else "unlocked")
    if is_locked:
        time.sleep(0.5)
    
    # set dpms timeout
    time_settings = DPMS_TIMES_LOCKED if is_locked else DPMS_TIMES_UNLOCKED
    times_as_strings = [str(x) for x in time_settings]
    run_command("xset", "dpms", *times_as_strings)

    # change the current state
    enabled_string = "off" if is_locked else "on"
    run_command("xset", "dpms", "force", enabled_string)


def run_command(*command: str) -> None:
    try:
        status = subprocess.call(command)
        print("[DEBUG]", command)
        if status != 0:
            print(f"Command exited with code {status}:", command)
    except KeyboardInterrupt:
        sys.exit()
    except:
        print("Error executing command:", command)
        traceback.print_exc()


def is_computer_locked() -> bool:
    try:
        return is_process_running(SCREEN_LOCKERS)
    except KeyboardInterrupt:
        sys.exit()
    except:
        traceback.print_exc()
        return False


def is_process_running(process_names: list[str]) -> bool:
    normalized_process_names = [x.lower() for x in process_names]
    # print(normalized_process_names)
    #Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            proc_name = proc.name().lower()
            # print(proc_name)
            if proc_name in normalized_process_names:
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False


if __name__ == "__main__":
    main()
