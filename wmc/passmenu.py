#!/usr/bin/env 62-bin-python
import argparse
import os
import sys
import subprocess
from subprocess import PIPE
from typing import Optional
import traceback
import re
# pip install dmenu
try:
    import dmenu
except:
    print("Missing dependency. Please install it with 'python3 -m pip install dmenu'")
    sys.exit(1)


# This commands will be executed and the username/password will be piped into it
PROGRAM = "xclip -selection clipboard -rmlastnl"


def notify(message: str) -> None:
    try:
        subprocess.call(["notify-send", "passmenu.py", message])
    except:
        traceback.print_exc()


def choose_entry(path: str) -> Optional[str]:
    os.chdir(path)

    entry_names = []
    for root, _, files in os.walk("."):
        for f in files:
            if f.endswith(".gpg"):
                # remove the leading "./" and the trailing ".gpg"
                full_name = os.path.join(root[2:], f[:-4])
                entry_names.append(full_name)

    return dmenu.show(entry_names, prompt="pass")


def main():
    vault_dir = os.path.expanduser("~/.password-store")
    entry_name = choose_entry(vault_dir)
    print(entry_name)

    try:
        pwd_file = subprocess.check_output(["pass", "show", entry_name])
    except Exception as ex:
        notify(f"Error executing 'pass show \"{entry_name}\"'")
        sys.exit(1)
    lines = pwd_file.decode("utf-8").rstrip().split("\n")
    print(lines)

    if not lines:
        notify(f"Entry '{entry_name}' is empty")
        sys.exit()
    else:
        choose_entry_line(entry_name, lines)


def choose_entry_line(entry_name: str, lines: list[str]):
    display_line_count = min(len(lines), 15)
    if len(lines) == 1:
        chosen_line = lines[0]
    else:
        chosen_line = dmenu.show(lines, prompt=entry_name, lines=display_line_count)

    if not chosen_line:
        return
    print(chosen_line)
    value = chosen_line
    match = re.match(r"^[\w-]+:\s+", value) #match something like "user:" or "e-mail:"
    if match:
        match_end = match.span()[1]
        value = value[match_end:]

    process = subprocess.Popen([PROGRAM], shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    process.stdin.write(value.encode("utf-8"))
    # process.communicate(value.encode("utf-8"), timeout=1.5)

    notify(f"Copied '{value}'")


if __name__ == "__main__":
    main()