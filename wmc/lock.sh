#!/usr/bin/env bash

DISABLE_FILE="/tmp/lock-on-inactivity.disabled"
NOTIFY_ON_STATUS_CHANGE=false

function action_do {
  if [ -z "$1" ]; then
    send-notification "lock.sh" "Missing argument for trigger"
  fi

  if [ ! -f "${DISABLE_FILE}" ]; then
    $1
  fi
}

function action_enable {
  rm -f "${DISABLE_FILE}"
  status=$?

  if [ "${NOTIFY_ON_STATUS_CHANGE}" = true ] ; then
    send-notification "Enabled lock on inactivity"
  fi

  return $status
}

function action_disable {
  touch "${DISABLE_FILE}"
  status=$?

  if [ "${NOTIFY_ON_STATUS_CHANGE}" = true ] ; then
    send-notification "Disabled lock on inactivity"
  fi

  return $status
}

function show_help {
  echo "Usage: <action>"
  echo
  echo "Possible actions:"
  echo " - trigger <command>: Lock if enabled, otherwise do nothing"
  echo " - status: Show whether this program is enabled (error -> disabled, success -> enabled)"
  echo " - enable / on: Enable screen locking"
  echo " - disable / off: Disable screen locking"
  echo " - toggle: Toggle between enabled and disabled"
}


case "$1" in
  trigger)
    action_do "$2"
    ;;

  status)
    if [ -f "${DISABLE_FILE}" ]; then
      echo "disabled"
      exit 1
    else
      echo "enabled"
      exit 0
    fi
    ;;

  toggle)
    if [ -f "${DISABLE_FILE}" ]; then
      action_enable
    else
      action_disable
    fi
    ;;

  enable | on)
    action_enable
    ;;

  disable | off)
    action_disable
    ;;

  *)
    show_help
    exit 1
    ;;
esac
