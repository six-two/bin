#!/usr/bin/env bash
# Serve local websites on a local port. The port is based on the user-id, so that it should work on multi-user systems

PORT=$((12000+$(id -u)))
SOURCE=~/.websites

is_empty_dir() {
    [[ -n "$(find "$1" -maxdepth 0 -empty)" ]]
}

if [[ -d "$SOURCE" ]]; then
    if is_empty_dir "$SOURCE"; then
        echo "[!] $SOURCE is empty, not starting web server"
        exit 1
    else
        echo "[*] Serving $SOURCE at 127.0.0.1:$PORT"
        python3 -m http.server --bind 127.0.0.1 --directory "$SOURCE" "$PORT"
    fi
else
    echo "[!] Directory $SOURCE does not exist"
    exit 1
fi
