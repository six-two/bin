#!/usr/bin/env bash
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

if [[ $# -ne 2 ]]; then
    echo "[!] Usage: $0 <PENTEST> <TARGET_SHELL>"
    echo "    Where <PENTEST> is '0' or '1' (1 means generate aliases for pentesting)"
    echo "    Where <TARGET_SHELL> is the name of the shell (like 'fish', 'bash' or 'zsh')"
    exit 2
fi
GEN_PENTEST="$1"
TARGET_SHELL="$2"

is_installed() {
    command -v "$1" &>/dev/null
}

# If supported (fish only), use abbreviations. Otherwise use aliases.
add_abbr() {
    if [[ "$#" -ne 2 ]]; then
        echo "[-] Usage error: add_abbr wants 2 args but has $#: $@" >&2
        exit 0
    fi
    if [[ "${TARGET_SHELL}" == fish ]]; then
        # We add an alias, so that tab completion works
        # We use an abbreviation, so that we can see the full command and edit it
        echo -e "abbr --add $1 '$2'\nalias $1='$2'"
    else
        echo "alias $1='$2'"
    fi
}

# Tab in fish is differnet (\t) than in zsh/bash ($'\t').
# But single quotes are hard to quote in single quotes, so I just delegate it to printf
if [[ "${TARGET_SHELL}" == fish ]]; then
    cat << "EOF"
alias align-by-tab='column --separator \t --table'
EOF
    # add autocd abbreviations
    add_abbr ac autocd
    add_abbr ach autocd-here
elif [[ "${TARGET_SHELL}" == bash || "${TARGET_SHELL}" == zsh ]]; then
    cat << "EOF"
alias align-by-tab='column --separator "$(printf "\t")" --table'
EOF
else
    cat << "EOF"
alias align-by-tab='column --separator "$(printf "\t")" --table'
EOF
    echo "[-] Unknown shell, not sure how tabs should be represented" >&2
fi

    add_abbr x 'chmod +x'

cat << "EOF"

################################################
############### Start of aliases ###############
################################################

alias remove-duplicates="awk '!x[\\\$0]++'"
alias to-upper='tr "[:lower:]" "[:upper:]"'
alias to-lower='tr "[:upper:]" "[:lower:]"'
alias add-commas='sed "\$ ! s/\$/,/"'
alias censor='sed "s/[[:alnum:]]/X/g"'
# Censor clipboard in-place and print it
alias cbcensor='cb | censor | tee /dev/stderr | cb'
alias count-and-sort='sort | uniq -c | sort -k1,1nr'
alias strip-comments-and-blank-lines='sed -e "/^#/d" -e "/^[[:space:]]*\$/d"'
alias strip-blank-lines='sed "/^[[:space:]]*\$/d"'
alias tsv2md='sed "s/\t/\t| /g" | align-by-tab'
alias newline-to-html-br='python3 -c "import sys; print(\"<br>\".join(sys.stdin.read().split(chr(10))))"'
# convert unicode to python repr strings and vice versa
alias repr='python3 -c "print(repr(__import__(\"sys\").stdin.buffer.read()))"'
alias unrepr='python3 -c "print(eval(__import__(\"sys\").stdin.read()).decode())"'

################ misc ################
# Make me the owner of a file (probably owned by root)
alias fix-owner='sudo chown "$USER:$USER"'

############### color command outputs ##########
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias diff='diff --color=auto'
alias ip='ip --color=auto'
EOF

add_abbr ll 'ls -l'
add_abbr la 'ls -lA'
add_abbr lt 'ls -ltr'
add_abbr f 'find . -iname'
add_abbr date-safe 'date +"%Y-%m-%d_%H-%M-%S"'
add_abbr date-iso 'date -Iseconds'
add_abbr own 'sudo chown "$USER:$USER"'


# Initials of Package Manager + <Install|Update|Search|Remove|File|Query>
if is_installed pacman; then
    add_abbr pmi 'sudo pacman -S'
    add_abbr pmu 'sudo pacman -Syu'
    add_abbr pms 'pacman -Ss'
    add_abbr pmr 'sudo pacman -Rns'
    add_abbr pmf 'pacman -F'
    add_abbr pmq 'pacman -Qi'
elif is_installed apt-get; then
    # check for apt-get, since MacOS has a program called apt
    add_abbr pmi 'sudo apt install'
    add_abbr pmu 'sudo apt update && sudo apt upgrade'
    add_abbr pms 'sudo apt search'
    add_abbr pmr 'sudo apt remove'
    add_abbr pmar 'sudo apt autoremove'
fi


# Initials of AUR Manager + <Install|Update|Search|Remove|File>
if is_installed pikaur; then
    add_abbr ami 'pikaur -S'
    add_abbr amu 'pikaur -Syu'
    add_abbr ams 'pikaur -Ss'
    add_abbr amr 'pikaur -Rns'
    add_abbr amf 'pikaur -F'
fi


if is_installed git; then
    # Character diff. This only works with git's diff version, not with the native one
    add_abbr cdiff 'git diff --no-index --color-words=.'
    add_abbr wdiff 'git diff --no-index --word-diff=color'

    add_abbr gaa 'git add .'
    add_abbr gbr 'git branch'
    add_abbr gca 'git commit --amend'
    add_abbr gcm 'git commit -m'
    add_abbr gco 'git checkout'
    add_abbr gd 'git diff'
    add_abbr gdl 'git diff HEAD~1 HEAD'
    add_abbr gds 'git diff --staged'
    add_abbr gph 'git push'
    add_abbr gpht 'git push --tags'
    add_abbr gpl 'git pull'
    add_abbr gplr 'git pull --rebase'
    add_abbr gsplr 'git stash && gplr && git stash pop'
    add_abbr gss 'git stash show'
    add_abbr grso 'git remote show origin'
    add_abbr gs 'git status'
    add_abbr gst 'git stash'
    add_abbr gstp 'git stash pop'

    # Taken from https://github.com/maurizi0/config-files/blob/e2f340618f4634214678672e48e79a3b3118aba7/.gitconfig#L10C10-L11C1
    # The only way to "escape" a single quote is to end a single quoted string ' add a quoted single quote "'" and then start a single quoted string again '. So ' -> '"'"'
    echo 'alias gl="git log --color --graph --pretty=format:'"'"'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'"'"' --abbrev-commit"'
fi


if is_installed screen; then
    add_abbr sl 'screen -list'
    add_abbr sr 'screen -d -r'
    add_abbr sf 'screen -s fish -S'
    add_abbr sz 'screen -s zsh -S'
fi


if is_installed python3; then
    add_abbr p3 'python3'
    add_abbr p3m 'python3 -m'
    add_abbr p3i 'python3 -m pip install'
    add_abbr p3ib 'python3 -m pip install --break-system-packages'
    add_abbr p3ir 'python3 -m pip install -r requirements.txt'
    add_abbr p3u 'python3 -m pip uninstall'
    add_abbr p3h 'python3 -m http.server --directory'

    cat << EOF
alias p3build='rm -r dist/; python3 -m build'
alias p3upload='python3 -m twine upload dist/*'
EOF
fi

if is_installed systemctl; then
    cat << EOF

################ systemctl ################
# usage: saction <service-name>
alias sstart='sudo systemctl start'
alias srestart='sudo systemctl restart'
alias sstop='sudo systemctl stop'

alias sstatus='systemctl status'
# show log messages since bootup for this service
alias slog='journalctl --boot --unit'

alias senable='sudo systemctl enable'
alias sdisable='sudo systemctl disable'
EOF
fi

if is_installed lsof; then
    add_abbr lstcp-user 'lsof -iTCP -sTCP:LISTEN -n -P'
    add_abbr lstcp-all 'sudo lsof -iTCP -sTCP:LISTEN -n -P'
    add_abbr lsudp-user 'lsof -iUDP -n -P'
    add_abbr lsudp-all 'sudo lsof -iUDP -n -P'
fi

# docker not installed, but podman is -> make docker an alias for podman
if is_installed podman-compose && ! is_installed docker-compose; then
    add_abbr docker-compose 'podman-compose'
fi
if is_installed docker || is_installed podman; then
    if is_installed podman && ! is_installed docker; then
        add_abbr docker 'podman'
    fi

    # Basic docker invocations
    add_abbr dr 'docker run --rm -it'
    add_abbr drs 'docker run --rm -it --entrypoint=sh'
    add_abbr drh 'docker run --rm -it -v "$PWD:/share" -w "/share"'
    add_abbr dro 'docker run --rm -it --network=none'
    add_abbr droh 'docker run --rm -it --network=none -v "$PWD:/share" -w "/share"'
    add_abbr drsh 'docker run --rm -it -v "$PWD:/share" -w "/share" --entrypoint=sh'

    # Special often used docker flags
    if [[ "${TARGET_SHELL}" == fish ]]; then
        echo "abbr --add --position anywhere -- dE '--entrypoint=sh'"
        echo "abbr --add --position anywhere -- dN '--network=none'"
        echo "abbr --add --position anywhere -- dV '-v \"\$PWD:/share\" -w \"/share\"'"
    fi

    # Some docker images
    add_abbr djohn 'docker run --rm -v "$PWD:/home/JtR/.john" -w /home/JtR/.john ghcr.io/openwall/john'
    add_abbr dffmpeg 'docker run --rm -v "$PWD:/share" -w /share jrottenberg/ffmpeg:5-vaapi'

    # My docker images
    add_abbr daws 'docker run --rm -it -v "$HOME/.aws:/root/.aws" -v "$PWD:/share" -w "/share" amazon/aws-cli'
    add_abbr dcsv2md 'docker run --rm -it -v "$PWD:/share" -w "/share" ghcr.io/six-two/csv2md'
    add_abbr dffmpeg-rubberband 'docker run -v "$PWD:/share" ghcr.io/six-two/ffmpeg-rubberband'
    add_abbr dffuf 'docker run --rm -it -v "$PWD:/share" -w "/share" ghcr.io/six-two/ffuf'
    add_abbr dhashid 'docker run --rm -it -v "$PWD:/share" -w "/share" ghcr.io/six-two/hashid'
    add_abbr dnmap 'docker run --rm -it -v "$PWD:/share" -w "/share" ghcr.io/six-two/nmap'
    add_abbr dnmapr 'docker run --rm -it -v "$PWD:/share" -w "/share" --cap-add=NET_RAW --cap-add=NET_ADMIN ghcr.io/six-two/nmap-rootless'
    add_abbr dpowerhub 'docker run --rm -it -v "$PWD:/share" -w "/share" ghcr.io/six-two/powerhub'
    add_abbr dscdl 'docker run --rm -it -v "$PWD:/share" -w "/share" ghcr.io/six-two/scdl'
    add_abbr dsourcemapper 'docker run --rm -it -v "$PWD:/share" -w "/share" ghcr.io/six-two/sourcemapper'
fi

# cypher-shell is installed but not in PATH: add a custom alias for it
if ! is_installed cypher-shell && [[ -x /usr/share/neo4j/bin/cypher-shell ]]; then
    echo "alias cypher-shell='/usr/share/neo4j/bin/cypher-shell'"
fi

if [[ $GEN_PENTEST -eq 1 ]]; then
    cat << EOF

################ pentest aliases ################
# Curl through local proxy (like burpsuite)
alias purl='curl --insecure --proxy http://127.0.0.1:8080'
# Color requests with PwnFox
alias pburl='purl -H "X-PwnFox-Color: blue"'
alias prurl='purl -H "X-PwnFox-Color: red"'
alias pgurl='purl -H "X-PwnFox-Color: green"'
alias pyurl='purl -H "X-PwnFox-Color: yellow"'
alias pourl='purl -H "X-PwnFox-Color: orange"'
# Tunnel a local tool through burp
alias burp='proxychains -f "$SCRIPT_DIR/pentest/proxychains-burp.conf"'
# Tunnel a local tool through socks4 proxy at 1080 (like cobalt strike SOCKS or SSH reverse socks)
alias pivot='proxychains -f "$SCRIPT_DIR/pentest/proxychains-pivot.conf"'
EOF
fi

if [[ "$(uname)" == Darwin ]]; then
    add_application_alias() {
        # Usage: <alias_name> <Application_Name>
        if [[ -d "/Applications/$2/" || -d "/System/Applications/$2/" || -d "/System/Applications/Utilities/$2/" ]]; then
            #echo "alias $1='open -a \"$2\"'"
            add_abbr "$1" "open -a \"$2\""
        fi
    }

    if [[ -f "/Applications/VMware Fusion.app/Contents/Library/vmcli" ]]; then
        add_abbr vmcli "/Applications/VMware\ Fusion.app/Contents/Library/vmcli"
    fi
    if [[ -f "/Applications/Visual Studio Code.app/Contents/Resources/app/bin/code" ]]; then
        add_abbr code "/Applications/Visual\ Studio\ Code.app/Contents/Resources/app/bin/code"
    fi


    add_application_alias burpsuite-pro "Burp Suite Professional.app"
    add_application_alias chrome "Google Chrome.app"
    add_application_alias excel "Microsoft Excel.app"
    add_application_alias firefox Firefox.app
    add_application_alias marta Marta.app
    add_application_alias pathfinder "Path Finder.app"
    add_application_alias phoenix-slides "Phoenix Slides.app"
    add_application_alias powerpoint "Microsoft PowerPoint.app"
    add_application_alias preview Preview.app
    add_application_alias safari Safari.app
    add_application_alias terminal Terminal.app
    add_application_alias textedit TextEdit.app
    add_application_alias vscode "Visual Studio Code.app"
    add_application_alias word "Microsoft Word.app"

    cat << EOF
alias wifi-ip="networksetup -getinfo Wi-Fi | awk '/^IP address:/ {print \$3}'"

# Sed inplace has a different usage on macOS, so we make an alias that works on both.
alias sedi='sed -i ""'
EOF

add_abbr sha1sum 'shasum -a 1'
add_abbr sha256sum 'shasum -a 256'
add_abbr sha384sum 'shasum -a 384'
add_abbr sha512sum 'shasum -a 512'


else
    cat << EOF

################ Linux aliases ################
alias sedi='sed -i'
EOF
fi

# @TODO: why is this not working in bash? 
if is_installed docker-compose; then
    # We include the dash to prevent conflict with the /usr/bin/dc calculator tool
    add_abbr dc- docker-compose

    if [[ -d ~/c/dockerfiles/docker-compose ]]; then
        # My dockerfiles repo is likely installed.
        # Generate an alias for using each compose file easily
        # Example usage: dc-languagetool-isolated up -d
        for DIR in ~/c/dockerfiles/docker-compose/*; do
            if [[ -d "$DIR" ]]; then
                NAME="$(basename "$DIR")"
                echo "alias dc-${NAME}='docker-compose -f \"$DIR/docker-compose.yml\"'"
            fi
        done
    fi
fi
