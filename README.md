# bin

This is a collection os small custom helper scripts and which some scripts which work around / fix bugs in official binaries.

## Setup

Optional: If you want to make sure that you have installed all zsh plugins, etc run:
```bash
./install-optional-dependencies.sh
```

Add the tools to your shell initialization files.
For zsh, fish and bash this can be done with:
```bash
./install.sh <flags_for_features_you_want>
```

To list which flags are available you can run:
```bash
./generate_config_to_source
```

Optional: You may also want to configure some environment variables to tweak some shell bahavior.
Run `62-bin-variables` to see an explanation of the variables and their current values.

## Configuration

If you want to convert all builtin shells to `fish` (like in Visual Studio Code, Marta, xfce4-terminal, etc) then set the `MYBINS_FISH_EVERYWHERE` to something in your default shell's configuration (`~/.bashrc` / `~/.zshrc`):

```bash
export MYBINS_FISH_EVERYWHERE=1
```

## Backup

### Setup
First you need to set up a backup partition.
That partition has to be named `BACKUP` (it should be available as `/dev/disk/by-partlabel/BACKUP`).
The partition can be encrypted with LUKS.

Then you need to mount the partition and create a folder called `backup`.
In that folder you should create a borg archive:

```
sudo borg init /path/to/mounted/partition/backup -e none
```

### Usage

Action | Command
---|---
Mount backup partition | `backup-disk mount`
Create an incremental backup (with borg) | `backup-borg`
Unmount backup partition | `backup-disk umount`

The above commands can be run by just executing `backup run`.
