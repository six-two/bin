# Configuration files for Marta macOS file manager

## Installation

### Via smylink

Keep local setting synced with this repository:
```bash
rm "$HOME/Library/Application Support/org.yanex.marta/conf.marco"; ln -s "$PWD/conf.marco" "$HOME/Library/Application Support/org.yanex.marta/conf.marco"
```

### Via copy

Any changes done locally or in this repo will not be synced/applied:
```bash
rm "$HOME/Library/Application Support/org.yanex.marta/conf.marco"; cp ./conf.marco "$HOME/Library/Application Support/org.yanex.marta/"
```
