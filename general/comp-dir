#!/usr/bin/env bash
# Compare the file names of each folder (not the contents)


is_installed() {
    command -v "$1" &>/dev/null
}

show_help_and_exit() {
    cat << EOF
Usage: $0 Folder_A Folder_B [MODE]

Compare folder contents
EOF
    exit 1
}

if [[ $# -lt 2 || $# -gt 3 ]]; then
    show_help_and_exit
fi

# Get absolute paths, since we will likely CD into directories
DIR_A="$(realpath "$1")"
DIR_B="$(realpath "$2")"
MODE="${3:-name}"

if [[ ! -d "$DIR_A" ]]; then
    echo "First directory does not exist (or you lack permission to access it): $DIR_A"
    exit 1
elif [[ ! -d "$DIR_B" ]]; then
    echo "Second directory does not exist (or you lack permission to access it): $DIR_B"
    exit 1
fi

main() {
    TMP_A="$(mktemp)"
    TMP_B="$(mktemp)"

    cd "$DIR_A" || exit 2
    get_dir_info > "$TMP_A"    

    cd "$DIR_B" || exit 2
    get_dir_info > "$TMP_B"    

    diff "$TMP_A" "$TMP_B" | less -S

    rm "$TMP_A" "$TMP_B"
}

get_dir_info() {
    # Usage: no args
    case "$MODE" in
    name)
        find .
        ;;
    contents)
        find . -type f -exec shasum "{}" ";"
        ;;
    *)
        echo -e "[!] Unknown mode: $MODE"
        exit 1
        ;;
    esac
}


main
