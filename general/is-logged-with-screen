#!/usr/bin/env bash
# Check whether a binary exists
if [[ $# -gt 1 ]]; then
    echo "Usage: $0 [PID]"
    exit 2
fi

get_parent_pid() {
    # Usage: <PID>, Returns PID of parent
    ps -p "$1" -o ppid= | sed 's/[^0-9]//g'
}

get_process_command() {
    # Usage: <PID>, Returns the full commandline of the process
    if [[ "$(uname)" == Linux ]]; then
        ps -p "$PID" -o cmd=
    elif [[ "$(uname)" == Darwin ]]; then
        ps -p "$PID" -o command=
    else
        echo "[$0] Unknown OS: $(uname)" >&2
    fi
}

# If no PID is given, use the PID of the process that called this command
if [[ $# -eq 0 ]]; then
    PID=$(get_parent_pid $$)
else
    PID="$1"
fi

# Get the parent of your target process
PID=$(get_parent_pid "$PID")

# Check if the process command starts with "script "
get_process_command "$PID" | grep -q "^script "
