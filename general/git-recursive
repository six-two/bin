#!/usr/bin/env python3
# Find git repos (and execute git command in them)
import argparse
import os
import subprocess
import sys
from typing import Callable

RED = "\033[91;1m"
BLUE = "\033[94;1m"
RESET = "\033[0m"

def find_git_repos(folder: str, callback: Callable[[str], None]) -> int:
    count = 0
    for root, dirs, _files in os.walk(folder):
        if ".git" in dirs:
            count += 1
            callback(root)
    return count


def main():
    prog = sys.argv[0]
    args = sys.argv[1:]
    if len(args) < 1:
        print("Execute git command: <search-path> <git-args...>")
        print(f"Example: {prog} $HOME pull")
        print("")
        print("List git repositories: <search-path>")
        print(f"Example: {prog} ~/repos/")
        sys.exit(1)
    else:
        folder = args[0]
        git_args = args[1:]

        if git_args:
            failed = []

            def callback(repo_path: str):
                line = f" {repo_path} ".center(80, "=")
                print(f"{BLUE}{line}{RESET}")
                code = subprocess.call(["git", "-C", repo_path, *git_args])
                if code != 0:
                    failed.append(repo_path)
            total = find_git_repos(folder, callback)

            print(f"{BLUE}{total} repo(s) found{RESET}")
            if failed:
                print(f"{RED}{len(failed)} errors encountered{RESET}")
        else:
            find_git_repos(folder, print)

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("[Ctrl-C]")
        sys.exit(1)
