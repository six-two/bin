#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
  echo -e "[*] This script needs root! Restarting with sudo"
  exec sudo "$0" "$@"
fi

get_decrypted_partitions() {
    for CRYPT_DEVICE in /dev/mapper/crypt_*; do
        # Work around the "/dev/mapper/crypt_*" returned bugw
        if [[ -e "${CRYPT_DEVICE}" ]]; then
            echo "${CRYPT_DEVICE}"
        fi
    done
}

# Exit if no devices found
if [[ "$(get_decrypted_partitions | wc -l)" -eq 0 ]]; then
    echo "[!] No LUKS partitions mounted with 'luks-mnt' found"
    exit 1
fi

# Let the user select the partition to unmount
DEVICE="$(get_decrypted_partitions | fzf -0)"
if [[ $? -ne 0 ]]; then
    echo "[!] fzf failed or canceled by user"
    exit 1
fi

# Unmount all mount points of the crypt device
## findmnt: Get all mount points where the crypt-device is mounted
## sed    : Remove the header line just saying "TARGET"
for MOUNT_POINT in $(findmnt "${DEVICE}" -o TARGET | sed 1d); do
    echo "[*] Unmounting ${MOUNT_POINT}"
    if ! umount "${MOUNT_POINT}"; then
        echo "[!] Unmount failed, the following programs seem to still use it:"
        lsof "${MOUNT_POINT}" 2>/dev/null
        exit 1
    fi
done

# Actually close the device
echo "[*] Closing LUKS device"
cryptsetup close "$(basename "${DEVICE}")"

# Check that it is really closed
if [[ -e "${DEVICE}" ]]; then
    echo "[!] Failed to close the crypt device. Make sure not programs are using the partition and try again"
    exit 1
fi
