#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
ZSH_REPO_DIR="${SCRIPT_DIR}/zsh/repos"

is_installed() {
    command -v "$1" &>/dev/null
}

install_on_arch_linux() {
    # These are the packages we want
    WANTED=$(cat <<'EOF'
bash-completion
fish
zsh
zsh-autosuggestions
zsh-completions
zsh-history-substring-search
zsh-syntax-highlighting
EOF
)

    # Then we determine, which packages are not yet installed
    MISSING="$(comm -23 \
        <(echo "${WANTED}" | sort -u) \
        <(pacman -Qi| grep "^Name" | cut -d ":" -f2 | sed 's/[[:space:]]//g' | sort -u)
    )"

    if [[ -n "${MISSING}" ]]; then
        echo "[*] The following pacman packages are not yet installed:" $MISSING
        echo "    Please provide your sudo password to install them."
        sudo pacman -S ${MISSING}
    else
        echo "[*] All pacman packages are already installed"
    fi
}


install_on_kali_linux() {
    # These are the packages we want
    WANTED=$(cat <<'EOF'
bash-completion
fish
zsh
zsh-autosuggestions
zsh-syntax-highlighting
EOF
)
    if [[ -n "${MISSING}" ]]; then
        echo "[*] The following apt packages are not yet installed:" $MISSING
        echo "    Please provide your sudo password to install them."
        sudo apt install ${MISSING}
    else
        echo "[*] All apt packages are already installed"
    fi

    clone_zsh_history_substring_search
}


install_on_generic_linux() {
    # Some random Linux distro, so IDK what package manager is used.
    # So we just tell the user what they need to install
    is_installed fish || echo "[*] The 'fish' shell is not installed. You may want to install it manually"
    is_installed zsh || echo "[*] The 'zsh' shell is not installed. You may want to install it manually"
    
    #@TODO clone repos: zsh-autosuggestions zsh-syntax-highlighting
    clone_zsh_history_substring_search
}

check_if_file_exists() {
  # Usage: <FILE_NAME> <LIKELY_PATH> [...MORE_LIKELY_PATHS]
  # Tries to find the given file in the given locations.

  if [[ $# -lt 2 ]]; then
    echo "Usage: check_if_file_exists <FILE_NAME> <LIKELY_PATH> [...MORE_LIKELY_PATHS]"
    return 2
  fi

  local NAME=$1
  shift

  for DIR in "$@"; do
    if [[ -f "${DIR}/${NAME}" ]]; then
      # File found
      return 0
    fi
  done

  # Not found
  return 1
}



clone_zsh_history_substring_search() {
    if ! check_if_file_exists zsh-history-substring-search.zsh \
            /usr/share/zsh/plugins/zsh-history-substring-search \
            "${ZSH_REPO_DIR}/repos/zsh-history-substring-search"; then
        # Clone the repo
        clone_zsh_repo https://github.com/zsh-users/zsh-history-substring-search
    fi
}


clone_zsh_repo() {
    if [[ -d "${ZSH_REPO_DIR}/$(basename "$1")" ]]; then
        echo "[*] Repo $(basename "$1") already exists"
    else
        echo "[*] Cloning zsh plugin git repo $(basename "$1")"
        [[ -d "${ZSH_REPO_DIR}" ]] || mkdir "${ZSH_REPO_DIR}"
        git clone "$1" "${ZSH_REPO_DIR}/$(basename "$1")"
    fi
}


if is_installed pacman; then
    # We are probably on an Arch Linux system or one of its related distros
    install_on_arch_linux
elif grep --quiet '^ID=kali$' /etc/os-release; then
    # We are on a Kali Linux system
    install_on_kali_linux
else
    # We are probably on some Linux system
    install_on_generic_linux
fi


# Use one shared venv for all script -> should be fine, not many dependencies are needed currently
echo "[*] Creating/updating a virtual environment"
python3 -m venv --clear --upgrade-deps "${SCRIPT_DIR}/venv"
source "${SCRIPT_DIR}/venv/bin/activate"
python3 -m pip install -r "${SCRIPT_DIR}/requirements.txt"
