#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

check_bash_scripts() {
    find "$SCRIPT_DIR" -type f -not -path '*/venv/*' -perm /u=x -exec grep '^#!.*bash$' {} /dev/null \; |
        cut -f1 -d: |
        xargs shellcheck --shell=bash
}


check_bash_scripts
